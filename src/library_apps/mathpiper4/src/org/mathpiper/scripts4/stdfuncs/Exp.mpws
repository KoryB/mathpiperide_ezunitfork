%mathpiper,def="Exp"

2 # Exp(x_Number?)::NumericMode?() <-- ExpNum(x);
4 # Exp(Ln(x_))           <-- x;
//110 # Exp(Complex(r_, i_)) <--  Exp(r)*(Cos(i) + I*Sin(i));
200 # Exp(0) <-- 1;
200 # Exp(-Infinity) <-- 0;
200 # Exp(Infinity) <-- Infinity;
200 # Exp(Undefined) <-- Undefined;

Exp(xlist_List?) <-- MapSingle("Exp",xlist);

%/mathpiper



%mathpiper_docs,name="Exp",categories="Mathematics Procedures;Calculus Related (Symbolic)"
*CMD Exp --- exponential function
*STD
*CALL
        Exp(x)

*PARMS

{x} -- argument to the procedure

*DESC

This procedure calculates $e$ raised to the power $x$, where $e$ is the
mathematic constant 2.71828... One can use {Exp(1)}
to represent $e$.

This procedure is threaded, meaning that if the argument {x} is a
list, the procedure is applied to all entries in the list.

*E.G.

In> Exp(0)
Result: 1;

In> Exp(I*Pi)
Result: -1;

In> NM(Exp(1))
Result: 2.7182818284;

*SEE Ln, Sin, Cos, Tan, NM
%/mathpiper_docs