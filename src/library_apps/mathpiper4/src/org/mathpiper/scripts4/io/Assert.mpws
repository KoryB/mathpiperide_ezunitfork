%mathpiper,def="Assert"

/// post an error if assertion fails
(Assert(errorclass_, errorobject_) predicate_) <--
{
        CheckErrorTableau();
        Decide(Equal?(predicate, True),        // if it does not evaluate to True, its an error
                True,
                {        // error occurred, need to post errorobject
                        Append!(GetErrorTableau(), [errorclass, errorobject]);
                        False;
                }
        );
}

/// interface
(Assert(errorclass_) predicate_) <-- Assert(errorclass, True) predicate;

/// interface
(Assert() predicate_) <-- Assert("generic", True) predicate;

%/mathpiper



%mathpiper_docs,name="Assert",categories="Programming Procedures;Error Reporting",access="private"
*CMD Assert --- signal "soft" custom error
*STD
*CALL
        Assert("str", expr) pred
        Assert("str") pred
        Assert() pred

*PARMS

{pred} -- predicate to check

{"str"} -- string to classify the error

{expr} -- expression, error object

*DESC

{Assert} is a global error reporting mechanism. It can be used to check for
errors and report them. An error is considered to occur when the predicate
{pred} evaluates to anything except {True}. In this case, the procedure returns
{False} and an error object is created and posted to the global error tableau.
Otherwise the procedure returns {True}.

Unlike the "hard" error procedure {Check}, the procedure {Assert} does not stop
the execution of the program.

The error object consists of the string {"str"} and an arbitrary
expression {expr}. The string should be used to classify the kind of error that
has occurred, for example "domain" or "format". The error object can be any 
expression that might be useful for handling the error later;
for example, a list of erroneous values and explanations.
The association list of error objects is currently obtainable through
the procedure {GetErrorTableau()}.

If the parameter {expr} is missing, {Assert} substitutes {True}. If both 
optional parameters {"str"} and {expr} are missing, {Assert} creates an error of class {"generic"}.

Errors can be handled by a
custom error handler in the portion of the code that is able to handle a certain class of
errors. The procedures {Error?}, {GetError} and {ClearError} can be used.

Normally, all errors posted to the error tableau during evaluation of an expression should
be eventually printed to the screen. This is the behavior of unparsers
{DefaultPrint}, {Print}, {UnparseMath2D} and {UnparseLatex} (but not of the
inline unparser, which is enabled by default); they call
{DumpErrors} after evaluating the expression.

*E.G.

In> Assert("bad value", "must be zero") 1=0
Result: False;

In> Assert("bad value", "must be one") 1=1
Result: True;

In> Error?()
Result: True;

In> Error?("bad value")
Result: True;

In> Error?("bad file")
Result: False;

In> GetError("bad value");
Result: "must be zero";

In> DumpErrors()
        Error: bad value: must be zero
Result: True;
No more errors left:

In> Error?()
Result: False;

In> DumpErrors()
Result: True;

*SEE Error?, DumpErrors, Check, GetError, ClearError, ClearErrors, GetErrorTableau

%/mathpiper_docs