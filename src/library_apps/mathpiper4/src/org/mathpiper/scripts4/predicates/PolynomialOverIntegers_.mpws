%mathpiper,def="PolynomialOverIntegers?"

10 # PolynomialOverIntegers?(expr_Procedure?) <-- 
{
    Local(x,vars);
    vars := VarList(expr);
    Decide(Length(vars)>?1,vars:=HeapSort(vars,"GreaterThan?"));    
    x := vars[1];
    PolynomialOverIntegers?(expr,x);
}

15 # PolynomialOverIntegers?(expr_) <-- False;


10 # PolynomialOverIntegers?(expr_,var_)::(CanBeUni(var,expr)) <--
{
    Decide( AllSatisfy?("Integer?",Coef(expr,var,0 .. Degree(expr,var))),
        True,
        False
      );
}

15 # PolynomialOverIntegers?(expr_,var_) <-- False;

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output


%mathpiper_docs,name="PolynomialOverIntegers?",categories="Programming Procedures;Predicates"

*CMD PolynomialOverIntegers? --- Check if [expr] is a polynomial in variable [var] all of whose coefficients are integers

*STD
*CALL
    PolynomialOverIntegers?(expr,var)

*PARMS

{expr} -- an algebraic expression which may be a polynomial

{var} -- a variable name which might be used in {expr}

*DESC

The command {PolynomialOverIntegers?} returns {True} if {expr} is a 
polynomial in {var} and all of its coefficients are integers.
It returns {False} if {expr} is not a polynomial in {var} or if any 
of its coefficients are not integers.

This can be important, since many factoring theorems are applicable 
to such polynomials but not others.

*E.G.


In> PolynomialOverIntegers?(2*x^3-3*x^2+5*x-14,x)
Result: True 


In> PolynomialOverIntegers?(2.0*x^3-3*x^2+5*x-14,x)
Result: False


In> PolynomialOverIntegers?(y^2-4)
Result: True
        NOTE: if variable name is omitted, a reasonable default is taken.


In> PolynomialOverIntegers?(x^2-a^2)
Result: False
        NOTE: the unbound variable 'a' need not be an integer.

%/mathpiper_docs


