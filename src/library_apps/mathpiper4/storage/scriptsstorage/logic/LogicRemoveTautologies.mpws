%mathpiper,def="LogicRemoveTautologies;SimpleNegate;LocalCmp;SimplifyExpression"

 // not clear is this will stay, but it is eq. to LogicSimplify(expr, 2)

1 # SimpleNegate(Not? (_x))  <-- x;
2 # SimpleNegate(_x)        <-- Not?(x);

/* LogicRemoveTautologies scans a list representing e1 Or? e2 Or? ... to find
   if there are elements p and  Not? p in the list. This signifies p Or? Not? p,
   which is always True. These pairs are removed. Another function that is used
   is RemoveDuplicates, which converts p Or? p into p.
*/

/* this can be optimized to walk through the lists a bit more efficiently and also take
care of duplicates in one pass */
LocalCmp(_e1, _e2)                  <-- LessThan?(PipeToString() Write(e1), PipeToString() Write(e2));

// we may want to add other expression simplifers for new expression types
100 # SimplifyExpression(_x)        <-- x;

// Return values:
//  [True] means True
//  [] means False
LogicRemoveTautologies(_e) <--
{
  Local(i, len, negationfound); Assign(len, Length(e));
  Assign(negationfound, False);

  //Echo(e);
  e := BubbleSort(e, "LocalCmp");

  For(Assign(i, 1), (i <=? len) And? (Not? negationfound), i++)
  {
    Local(x, n, j);
    // we can register other simplification rules for expressions
    //e[i] := MathNth(e,i) /:: [gamma(_y) <- SimplifyExpression(gamma(y))];
    Assign(x, MathNth(e,i));
    Assign(n, SimpleNegate(x));                    /* this is all we have to do because of
                                                the kind of expressions we can have coming in */

    For(Assign(j, i+1), (j <=? len) And? (Not? negationfound), j++) {
        Local(y);
        Assign(y, MathNth(e,j));

        Decide(Equal?(y, n),
            {
                //Echo(["Deleting from ", e, " i=", i, ", j=", j, Nl()]);

                Assign(negationfound, True);
                //Echo(["Removing clause ", i, Nl()]);
            },
        Decide(Equal?(y, x),
            {
                //Echo(["Deleting from ", e, " j=", j, Nl()]);
                DestructiveDelete(e, j);
                Assign(len,SubtractN(len,1));
            })
        );
    };
    Check(len =? Length(e), "Math", "The length computation is incorrect");
  };

  Decide(negationfound, [True], e);            /* note that a list is returned */
};

%/mathpiper