%mathpiper,def="Diverge"

RulebaseHoldArguments("Diverge", [aFunc, aBasis]);
RuleHoldArguments("Diverge", 2, 1, List?(aBasis) And? List?(aFunc) And? Length(aBasis) =? Length(aFunc))
        Add(Map("Differentiate", [aBasis,aFunc]));

%/mathpiper



%mathpiper_docs,name="Diverge",categories="Mathematics Functions;Calculus Related (Symbolic)"
*CMD Diverge --- divergence of a vector field
*STD
*CALL
        Diverge(vector, basis)

*PARMS

{vector} -- vector field to calculate the divergence of

{basis} -- list of variables forming the basis

*DESC

This function calculates the divergence of the vector field "vector"
with respect to the variables "basis". The divergence is defined as

        Diverge(f,x) = Differentiate(x[1]) f[1] + ...
            + Differentiate(x[n]) f[n],
where {n} is the length of the lists "vector" and
"basis". These lists should have equal length.

*E.G.

In> Diverge([x*y,x*y,x*y],[x,y,z])
Result: y+x;

*SEE D, Curl
%/mathpiper_docs





%mathpiper,name="Diverge",subtype="automatic_test"

/* One place where we forgot to change Sum to Add */
TestMathPiper(Diverge([x*y,x*y,x*y],[x,y,z]),x+y);

%/mathpiper