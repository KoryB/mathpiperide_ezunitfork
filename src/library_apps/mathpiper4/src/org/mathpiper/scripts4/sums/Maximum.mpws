%mathpiper,def="Maximum"

/*  this is disabled because some procedures seem to implicitly define Min / Max with a different number of args, and then MathPiper is confused if it hasn't loaded all the Procedure() declarations beforehand.
FIXME
/// Min, Max with many arguments
*/

//Procedure() Maximum(list);

//Procedure() Maximum(l1, l2);

Procedure() Maximum("l1", "l2", "l3", ...);



10 # Maximum(l1_, l2_, l3_List?) <-- Maximum(Concat([l1, l2], l3));
20 # Maximum(l1_, l2_, l3_) <-- Maximum([l1, l2, l3]);
/**/

10 # Maximum(l1_List?,l2_List?) <-- Map("Maximum",[l1,l2]);


20 # Maximum(l1_RationalOrNumber?,l2_RationalOrNumber?) <-- Decide(l1>?l2,l1,l2);


30 # Maximum(l1_Constant?,l2_Constant?) <-- Decide(NM(Eval(l1-l2))>?0,l1,l2);

// Max on empty lists
10 # Maximum([]) <-- Undefined;


20 # Maximum(list_List?) <--
{
  Local(result);
  result:= list[1];
  ForEach(item,Rest(list)) result:=Maximum(result,item);
  result;
}


30 # Maximum(x_) <-- x;

%/mathpiper



%mathpiper_docs,name="Maximum",categories="Mathematics Procedures;Numbers (Operations)"
*CMD Maximum --- maximum of a number of values
*STD
*CALL
        Maximum(x,y)
        Maximum(list)

*PARMS

{x}, {y} -- pair of values to determine the maximum of

{list} -- list of values from which the maximum is sought

*DESC

This procedure returns the maximum value of its argument(s). If the
first calling sequence is used, the larger of "x" and "y" is
returned. If one uses the second form, the largest of the entries in
"list" is returned. In both cases, this procedure can only be used
with numerical values and not with symbolic arguments.

*E.G.

In> Maximum(2,3);
Result: 3;

In> Maximum([5,8,4]);
Result: 8;

*SEE Minimum, Sum
%/mathpiper_docs