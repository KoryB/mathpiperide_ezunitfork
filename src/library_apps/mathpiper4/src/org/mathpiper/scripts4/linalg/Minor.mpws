%mathpiper,def="Minor"

Minor(matrix,i,j) := CoFactor(matrix,i,j)*(-1)^(i+j);

%/mathpiper

    %output,mpversion=".204",preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="Minor",categories="Mathematics Procedures;Linear Algebra"
*CMD Minor --- get principal minor of a matrix
*STD
*CALL
        Minor(M,i,j)

*PARMS

{M} -- a matrix

{i}, {j} - positive integers

*DESC

Minor returns the minor of a matrix around
the element ($i$, $j$). The minor is the determinant of the matrix obtained from $M$ by
deleting the $i$-th row and the $j$-th column.

*E.G.

In> A := [[1,2,3], [4,5,6], [7,8,9]];
Result: [[1,2,3],[4,5,6],[7,8,9]];

In> UnparseMath2D(A);
        
        /                    \
        | ( 1 ) ( 2 ) ( 3 )  |
        |                    |
        | ( 4 ) ( 5 ) ( 6 )  |
        |                    |
        | ( 7 ) ( 8 ) ( 9 )  |
        \                    /
Result: True;

In> Minor(A,1,2);
Result: -6;

In> Determinant([[2,3], [8,9]]);
Result: -6;

*SEE CoFactor, Determinant, Inverse
%/mathpiper_docs







%mathpiper,name="Minor",subtype="automatic_test"

{
    Local(ll);
    ll:=[ [1,2,3],
          [2,-1,4],
          [3,4,3]
        ];

    Verify(NM(Minor(ll,1,2)),-6);
}

%/mathpiper