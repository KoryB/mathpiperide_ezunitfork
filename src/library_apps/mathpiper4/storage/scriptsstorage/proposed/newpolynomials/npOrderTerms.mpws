%mathpiper,def="npOrderTerms;LEXgt;LEXlt;GRLEXgt;GRLEXlt;GREVLEXgt;GREVLEXlt"

Retract("npOrderTerms",*);
Retract("LEXgt",*);
Retract("LEXlt",*);
Retract("GRLEXgt",*);
Retract("GRLEXlt",*);
Retract("GREVLEXgt",*);
Retract("GREVLEXlt",*);
 
10 # npOrderTerms( expr_NP?, _termOrder ) <--
 {
    //---------------------------------------------------------------------
    // INPUT:  a polynomial, in the Sparse representation, with Markers
    // OUTPUT: the polynomial, in the Sparse representation, with Markers,
    //         with the terms reordered in specified term Order.
    //---------------------------------------------------------------------
    Decide(InVerboseMode(),{Tell("npOrderTerms",expr); Tell("       ",termOrder);});
    Local(np,v,out);
    v   := expr[2];
    np  := npTerms(expr);
    out := NP~v~npOrderTerms(np,termOrder);
   };
];
 
10 # npOrderTerms( expr_List?, _termOrder ) <--
 {
    //---------------------------------------------------------------------
    // INPUT:  a polynomial, in the Sparse representation, NO Markers
    // OUTPUT: the polynomial, in the Sparse representation, NO Markers,
    //         with the terms reordered in specified term Order.
    //---------------------------------------------------------------------
    Decide(InVerboseMode(),{Tell("npOrderTerms",expr); Tell("       ",termOrder);});
    Local(np,out);
    np := FlatCopy(expr);
    
    If( termOrder =? LEX )
      {
          Decide(InVerboseMode(),Tell("    ordering terms by LEX"));
          out := Sort( np, LEXgt );
      }
    Else If( termOrder =? GRLEX )
      {
          Decide(InVerboseMode(),Tell("    ordering terms by GRLEX"));    
          out := Sort( np, GRLEXgt );
      }
    Else If( termOrder =? GREVLEX )
      {
          Decide(InVerboseMode(),Tell("    ordering terms by GREVLEX"));
          out := Sort( np, GREVLEXgt );
      }
    Else 
      { Check(False,"TermOrder","npOrderTerms should never reach this point"); };

    out;
   };
];
 
20 # npOrderTerms( expr_NP? )   <-- npOrderTerms(expr,LEX);
 
25 # npOrderTerms( expr_List? ) <-- npOrderTerms(expr,LEX);


 // ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
 //  Below are the basic comparison operators (equivalent to '>?" and "<?")
 //  which determine how Terms compare under the various specified Term
 //  orders.  When we speak of "comparing terms", we are actually talking
 //  about taking a Sparse representation of a Term (e.g., [3,2,1,1], for 
 //  the term 3*x^2*y*z), and comparing it with the sparse representation
 //  of another term (say, [-1,1,2,2], for -x*y^2*z^2).  The actual work 
 //  of comparison ignores the coefficients (first element) and compares
 //  only the "Power-Products" -- the Rest of the list for each term.
 // ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
 
10 # LEXgt(term1_List?,term2_List?)_(Length(term1)=?Length(term2)) <--
 {
     Local(nonzero);
     nonzero := Select(Rest(term1-term2),"NotZero?");
     Length(nonzero) !=? 0 And? First(nonzero) >? 0;
 }; 

10 # LEXlt(term1_List?,term2_List?)_(Length(term1)=?Length(term2)) <--
 {
     Local(nonzero);
     nonzero := Select(Rest(term1-term2),"NotZero?");
     Length(nonzero) !=? 0 And? First(nonzero) <? 0;
 }; 

10 # GRLEXgt(term1_List?,term2_List?)_(Length(term1)=?Length(term2)) <--
 {
     Local(sum1,sum2,nonzero);
     sum1 := Sum(Rest(term1));    sum2 := Sum(Rest(term2));
     nonzero := Select(Rest(term1-term2),"NotZero?");
     Decide(sum1 >? sum2, True,
       Decide(sum1=?sum2 And? (Length(nonzero)!=?0 And? First(nonzero)>?0),True,False)
     );
 }; 

10 # GRLEXlt(term1_List?,term2_List?)_(Length(term1)=?Length(term2)) <--
 {
     Local(sum1,sum2,nonzero);
     sum1 := Sum(Rest(term1));    sum2 := Sum(Rest(term2));
     nonzero := Select(Rest(term1-term2),"NotZero?");
     Decide(sum1 <? sum2, True,
       Decide(sum1=?sum2 And? (Length(nonzero)!=?0 And? First(nonzero)<?0),True,False)
     );
 }; 

10 # GREVLEXgt(term1_List?,term2_List?)_(Length(term1)=?Length(term2)) <--
 {
     Local(sum1,sum2,revnonzero);
     sum1 := Sum(Rest(term1));    sum2 := Sum(Rest(term2));
     revnonzero := Reverse(Select(Rest(term1-term2),"NotZero?"));
     Decide(sum1 >? sum2, True,
       Decide(sum1=?sum2 And? (Length(revnonzero)!=?0 And? First(revnonzero)<?0),True,False)
     );
 }; 

10 # GREVLEXlt(term1_List?,term2_List?)_(Length(term1)=?Length(term2)) <--
 {
     Local(sum1,sum2,revnonzero);
     sum1 := Sum(Rest(term1));    sum2 := Sum(Rest(term2));
     revnonzero := Reverse(Select(Rest(term1-term2),"NotZero?"));
     Decide(sum1 <? sum2, True,
       Decide(sum1=?sum2 And? (Length(revnonzero)!=?0 And? First(revnonzero)>?0),True,False)
     );
 }; 

%/mathpiper

    
    

   

%mathpiper_docs,def="npOrderTerms",categories="Mathematics Functions;Polynomials"
*CMD npOrderTerms --- Sets the Term Ordering to be used with the New Polynomial
*CALL
        npOrderTerms(newPoly,termOrder)
        npOrderTerms(newPoly).
        
*PARMS
{newPoly}   -- a NewPolynomial as created by {MakeNewPoly()}
{termOrder} -- the Name of the term ordering to be used

*DESC
Given a newPolynomial such as $$[NP,[x,y,z],[2,1,2,0],[-3,0,1,1]]$$,
this function specifies which of the acceptable term ordering modes
is to be used with this polynomial.

Terms ordering is meaningless for univariate polynomials, but is very
important for multivariate polynomials, because it specifies how the
predicates {>} and {<} are to be interpreted with respect to the individual
terms (actually, the power-products) in this polynomial.  

Please consult a book or an article on multivariate polynomial arithmetic
to learn the reasons why this is significant, and the specific meanings and
applications of the acceptable term orderings.

Currently, The available acceptable Term Orderings are
{LEX},
{GRLEX},
{GREVLEX}

NOTE:  If {termOrder} is not specified, the {default} Term Ordering is {LEX}.

*E.G.

In> poly1 := MakeNewPoly(3*y^3-2*x^2+5*z+7);
Result: [NP,[x,y,z],[-2,2,0,0],[3,0,3,0],[5,0,0,1],[7,0,0,0]]


In> p1lex := npOrderTerms(poly1,LEX);
Result: [NP,[x,y,z],[-2,2,0,0],[3,0,3,0],[5,0,0,1],[7,0,0,0]]


In> p1grlex := npOrderTerms(poly1,GRLEX);
Result: [NP,[x,y,z],[3,0,3,0],[-2,2,0,0],[5,0,0,1],[7,0,0,0]]


In> p1grevlex := npOrderTerms(poly1,GREVLEX)
Result: [NP,[x,y,z],[3,0,3,0],[-2,2,0,0],[5,0,0,1],[7,0,0,0]]

*SEE MakeNewPoly
%/mathpiper_docs

    

    

  

%mathpiper_docs,def="Term Comparison Operators",categories="Mathematics Functions;Polynomials"
*CMD see below -- compares two terms according to specified Term Ordering
*CALL
        LEXgt(term1,term2)
        LEXlt(term1,term2)
        GRLEXgt(term1,term2)
        GRLEXlt(term1,term2)
        GREVLEXgt(term1,term2)
        GREVLEXlt(term1,term2)
        
*PARMS
{term1} -- a single term
{term2} -- another single term

*DESC
LEXgt(term1,term2) returns {True} if term1 > term2 according to LEX ordering.

LEXlt(term1,term2) returns {True} if term1 < term2 according to LEX ordering.

GRLEXgt(term1,term2) returns {True} if term1 > term2 according to GRLEX ordering.

GRLEXlt(term1,term2) returns {True} if term1 < term2 according to GRLEX ordering.

GREVLEXgt(term1,term2) returns {True} if term1 > term2 according to GREVLEX ordering.

GREVLEXlt(term1,term2) returns {True} if term1 < term2 according to GREVLEX ordering.

*E.G.


In> LEXgt([-2,2,0,0],[3,0,3,0])
Result: True


In> GRLEXgt([-2,2,0,0],[3,0,3,0])
Result: False


In> GREVLEXgt([-2,2,0,0],[3,0,3,0])
Result: False
*SEE MakeNewPoly, npOrderTerms
%/mathpiper_docs


