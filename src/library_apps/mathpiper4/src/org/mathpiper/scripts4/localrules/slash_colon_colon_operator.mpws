%mathpiper,def="/::"



10 # (expression_ /:: patterns_) <--
{
  Local(old);
  
  Assign(patterns, PatternsCompile(patterns));
  
  Assign(old, expression);
  
  Assign(expression, MacroSubstituteApply(expression,"LocPredicate","LocChange"));
  
  While (expression !=? old)
  {
    Assign(old, expression);
    
    Assign(expression, MacroSubstituteApply(expression,"LocPredicate","LocChange"));
  }
  
  expression;
}


%/mathpiper



%mathpiper_docs,name="/::",categories="Operators"
*CMD /:: --- local transformation rules

    expressions /:: patterns


*PARMS

{expression} -- an expression

{patterns} -- a list of patterns

*DESC

It might be necessary sometimes to use the {/::} operator, which
repeatedly applies the {/:} operator until the result doesn't change
any more. Caution is required, since rules can contradict each other,
which could result in an infinite loop. To detect this situation,
just use /: repeatedly on the expression. The repetitive nature
should become apparent.


*E.G.

In> Sin(u)*Ln(a*b) /:: [ a <- 2, b <- 3 ]
Result: Sin(u)*Ln(6);

*SEE /:, Substitute
%/mathpiper_docs