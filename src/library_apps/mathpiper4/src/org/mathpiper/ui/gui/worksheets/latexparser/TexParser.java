/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
//}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.ui.gui.worksheets.latexparser;

public class TexParser
{

    private boolean isJuxtaposition = false;
    private boolean showToken = false;
    

    private static String singleOps = "^_+=,";
    private int currentPos;
    private String iCurrentExpression;
    private String nextToken;

    private TexStack builder;

    public TexParser(TexStack builder, boolean isJuxtaposition)
    {
        this.builder = builder;
        this.isJuxtaposition = isJuxtaposition;
    }

    private void showToken(String sourceName)
    {
        System.out.println(sourceName + ": " + nextToken);
    }

    void nextToken()
    {
        nextToken = "";

        if (currentPos == iCurrentExpression.length())
        {
            if (showToken)
            {
                showToken("End of expression");
            }

            return;
        }

        while (currentPos < iCurrentExpression.length() && isSpace(iCurrentExpression.charAt(currentPos)))
        {
            //Skip spaces.
            currentPos++;
        }

        if (currentPos == iCurrentExpression.length())
        {
            //Return if at end of expression.

            if (showToken)
            {
                showToken("End of expression");
            }
            return;
        }
        else if (!isJuxtaposition && isAlNum(iCurrentExpression.charAt(currentPos)))
        {
            int startPos = currentPos;

            while (currentPos < iCurrentExpression.length() && isAlNum(iCurrentExpression.charAt(currentPos)))
            {
                currentPos++;
            }

            nextToken = iCurrentExpression.substring(startPos, currentPos);

            if (showToken)
            {
                showToken("Is alpha numeric");
            }
            return;
        }
        else if (isNum(iCurrentExpression.charAt(currentPos)))
        {
            int startPos = currentPos;

            while (currentPos < iCurrentExpression.length() && isNum(iCurrentExpression.charAt(currentPos)))
            {
                currentPos++;
            }

            nextToken = iCurrentExpression.substring(startPos, currentPos);

            if (showToken)
            {
                showToken("Is numeric");
            }
            return;
        }
        else if (isAlNum(iCurrentExpression.charAt(currentPos)))
        {
            int startPos = currentPos;

            nextToken = iCurrentExpression.substring(startPos, currentPos + 1);

            currentPos++;

            if (showToken)
            {
                showToken("Is alpha");
            }
            return;
        }

        int c = iCurrentExpression.charAt(currentPos);

        if (c == '{')
        {
            nextToken = "{";
            currentPos++;

            if (showToken)
            {
                showToken("Left brace");
            }
            return;
        }
        else if (c == '}')
        {
            nextToken = "}";
            currentPos++;

            if (showToken)
            {
                showToken("Right brace");
            }
            return;
        }
        
        if (c == '(')
        {
            nextToken = "(";
            currentPos++;

            if (showToken)
            {
                showToken("Left parentheses");
            }
            return;
        }
        else if (c == ')')
        {
            nextToken = ")";
            currentPos++;

            if (showToken)
            {
                showToken("Right parentheses");
            }
            return;
        }
        
        if (c == '[')
        {
            nextToken = "[";
            currentPos++;

            if (showToken)
            {
                showToken("Left bracket");
            }
            return;
        }
        else if ( c == ']')
        {
            nextToken = "]";
            currentPos++;

            if (showToken)
            {
                showToken("Right bracket");
            }
            return;
        }
        
        

        if (c == '|')
        {
            nextToken = "|";
            currentPos++;

            if (showToken)
            {
                showToken("Left or right abs");
            }
            return;
        }
        else if (singleOps.indexOf(c) >= 0)
        {
            nextToken = "" + (char) c;
            currentPos++;

            if (showToken)
            {
                showToken("Single operator");
            }
            return;
        }
        else if (c == '\\')
        {

            int startPos = currentPos;

            while (currentPos < iCurrentExpression.length() && (isAlNum(iCurrentExpression.charAt(currentPos)) || iCurrentExpression.charAt(currentPos) == '\\'))
            {
                currentPos++;
            }

            nextToken = iCurrentExpression.substring(startPos, currentPos);

            if (showToken)
            {
                showToken("Backslash");
            }
            return;
        }

        if (showToken)
        {
            showToken("No match");
        }
    }

    boolean matchToken(String token)
    {

        if (nextToken.equals(token))

        {
            return true;
        }

        System.out.println("Found " + nextToken + ", expected " + token);

        return false;
    }

    public Object parse(String aExpression) throws Throwable
    {
        iCurrentExpression = aExpression;
        currentPos = 0;
        nextToken();

        return parseTopExpression();
    }

    Object parseTopExpression() throws Throwable
    {

        parseOneExpression10(builder);

        Object expression = builder.pop();

        return expression;
    }

    void parseOneExpression10(TexStack builder) throws Throwable
    {
        parseOneExpression20(builder);

        // = ,
        while (nextToken.equals("=") || nextToken.equals("\\neq") || nextToken.equals(","))
        {

            String token = nextToken;

            nextToken();
            parseOneExpression20(builder);
            builder.process(token);
        }
    }

    void parseOneExpression20(TexStack builder) throws Throwable
    {
        parseOneExpression25(builder);

        // +, -
        while (nextToken.equals("+") || nextToken.equals("-") || nextToken.equals("\\wedge") || nextToken.equals("\\vee") || nextToken.equals("<") || nextToken.equals(">") || nextToken.equals("\\leq") || nextToken.equals("\\geq"))
        {

            String token = nextToken;

            if (token.equals("-"))
            {
                token = "-/2";
            }

            nextToken();
            parseOneExpression25(builder);
            builder.process(token);
        }
    }

    void parseOneExpression25(TexStack builder) throws Throwable
    {
        parseOneExpression30(builder);
        
        while (nextToken.equals("*") || nextToken.equals("\\times") || nextToken.equals("\\cdot"))
        {
            nextToken();
            parseOneExpression30(builder);
            builder.process("*");
        }

        // implicit *  todo:tk
        while (nextToken.length() > 0 && 
                !nextToken.equals("+") && 
                !nextToken.equals("-") && 
                !nextToken.equals("=") && 
                !nextToken.equals("\\neq") && 
                !nextToken.equals("}") && 
                !nextToken.equals(")") &&
                !nextToken.equals("]") &&
                !nextToken.equals("&") && 
                !nextToken.equals("\\wedge") && 
                !nextToken.equals("\\vee") && 
                !nextToken.equals("<") && 
                !nextToken.equals(">") && 
                !nextToken.equals("\\leq") && 
                !nextToken.equals("\\geq") && 
                !nextToken.equals("\\end") && 
                !nextToken.equals("\\\\") && 
                !nextToken.equals("\\right)") && 
                !nextToken.equals("\\right]") && 
                !nextToken.equals(",") && 
                !nextToken.equals("/") && 
                !nextToken.equals("\\div") &&
                !nextToken.equals("|") &&
                !nextToken.equals("*") &&
                !nextToken.equals("\\times") &&
                !nextToken.equals("\\cdot")
                )
        {

            //System.out.println("nextToken = "+nextToken);
            String token = "*";
            parseOneExpression30(builder);

            //System.out.println("After: nextToken = "+nextToken);
            builder.process(token);
        }


    }

    void parseOneExpression30(TexStack builder) throws Throwable
    {
        parseOneExpression40(builder);

        while (nextToken.equals("_") || nextToken.equals("^") || nextToken.equals("!") || nextToken.equals("/") || nextToken.equals("\\div"))
        {
            if (nextToken.equals("!"))
            {
                builder.process(nextToken);
                nextToken();
            }
            else
            {
                String token = nextToken;
                nextToken();
                parseOneExpression40(builder);
                builder.process(token);
            }
        }
    }

    void parseOneExpression40(TexStack builder) throws Throwable
    {

        // atom
        if (nextToken.equals("{"))
        {
            nextToken();
            parseOneExpression10(builder);

            if (!nextToken.equals("}"))
            {
                System.out.println("Got " + nextToken + ", expected }");

                return;
            }
        }
        else if (nextToken.equals("("))
        {
            nextToken();
            parseOneExpression10(builder);

            if (!nextToken.equals(")"))
            {
                System.out.println("Got " + nextToken + ", expected }");

                return;
            }
        }
        else if (nextToken.equals("["))
        {
            nextToken();
            parseOneExpression10(builder);

            if (!nextToken.equals("]"))
            {
                System.out.println("Got " + nextToken + ", expected }");

                return;
            }
        }
        else if (nextToken.equals("\\left("))
        {
            nextToken();
            parseOneExpression10(builder);

            if (!nextToken.equals("\\right)"))
            {
                System.out.println("Got " + nextToken + ", expected \\right)");

                return;
            }

            builder.process("[roundBracket]");
        }
        else if (nextToken.equals("|"))
        {
            nextToken();
            parseOneExpression10(builder);

            if (!nextToken.equals("|"))
            {
                System.out.println("Got " + nextToken + ", expected |");

                return;
            }

            builder.process("[absBracket]");
        }
        else if (nextToken.equals("\\left["))
        {
            nextToken();
            parseOneExpression10(builder);

            if (!nextToken.equals("\\right]"))
            {
                System.out.println("Got " + nextToken + ", expected \\right]");

                return;
            }

            builder.process("[squareBracket]");
        }
        else if (nextToken.equals("\\sqrt"))
        {
            nextToken();
            parseOneExpression30(builder);
            builder.process("[sqrt]");

            return;
        }
        else if (nextToken.equals("\\exp"))
        {
            nextToken();
            builder.process("e");
            parseOneExpression40(builder);
            builder.process("^");

            return;
        }
        else if (nextToken.equals("\\imath"))
        {
            builder.process("i");
        }
        else if (nextToken.equals("\\mathrm"))
        {
            nextToken();

            if (!matchToken("{"))

            {
                return;
            }

            int startPos = currentPos;

            while (currentPos < iCurrentExpression.length() && iCurrentExpression.charAt(currentPos) != '}')
            {
                currentPos++;
            }

            String literal = iCurrentExpression.substring(startPos, currentPos);
            currentPos++;
            builder.processLiteral(literal);
            nextToken();

            return;
        }
        else if (nextToken.equals("-"))
        {
            nextToken();
            parseOneExpression30(builder);
            builder.process("-/1");

            return;
        }
        else if (nextToken.equals("\\neg"))
        {
            nextToken();
            parseOneExpression30(builder);
            builder.process("~");

            return;
        }
        else if (nextToken.equals("\\sum"))
        {
            builder.process("[sum]");
        }
        else if (nextToken.equals("\\pi"))
        {
            builder.process("[pi]");
        }
        else if (nextToken.equals("\\int"))
        {
            builder.process("[int]");
        }
        else if (nextToken.equals("\\frac"))
        {
            nextToken();
            parseOneExpression40(builder);
            parseOneExpression40(builder);
            builder.process("/");

            return;
        }
        else if (nextToken.equals("\\begin"))
        {
            nextToken();

            if (!matchToken("{"))

            {
                return;
            }

            nextToken();

            String name = nextToken;
            nextToken();

            if (!matchToken("}"))

            {
                return;
            }

            if (name.equals("array"))
            {

                int nrColumns = 0;
                int nrRows = 0;
                nextToken();

                if (!matchToken("{"))

                {
                    return;
                }

                nextToken();

                String coldef = nextToken;
                nextToken();

                if (!matchToken("}"))

                {
                    return;
                }

                nrColumns = coldef.length();
                nrRows = 1;
                nextToken();

                while (!nextToken.equals("\\end"))
                {
                    parseOneExpression10(builder);

                    if (nextToken.equals("\\\\"))
                    {
                        nrRows++;
                        nextToken();
                    }
                    else if (nextToken.equals("&"))
                    {
                        nextToken();
                    }
                    else
                    {

                        //  System.out.println("END? "+nextToken);
                    }
                }

                nextToken();

                if (!matchToken("{"))

                {
                    return;
                }

                nextToken();

                String name2 = nextToken;
                nextToken();

                if (!matchToken("}"))

                {
                    return;
                }

                if (name2.equals("array"))
                {
                    builder.process("" + nrRows);
                    builder.process("" + nrColumns);
                    builder.process("[grid]");
                }
            }
        }
        else
        {
            builder.process(nextToken);
        }

        nextToken();
    }

    boolean isSpace(int c)
    {

        if (c == ' ' || c == '\t' || c == '\r' || c == '\n')

        {
            return true;
        }

        return false;
    }

    boolean isAlNum(int c)
    {

        if (isSpace(c))
        {
            return false;
        }

        if (c == '{' || c == '('  || c == '[')
        {
            return false;
        }

        if (c == '}' || c == ')'|| c == ']')
        {
            return false;
        }
        
        if (c == '|')
        {
            return false;
        }

        if (c == '\\')
        {
            return false;
        }

        if (singleOps.indexOf(c) >= 0)
        {
            return false;
        }

        return true;
    }

    boolean isNum(int c)
    {
        if ((c >= 48 && c <= 57) || (c == 46))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    

}
