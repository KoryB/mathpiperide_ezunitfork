%mathpiper,def="CUnparsable?"

//////////////////////////////////////////////////
/// CUnparsable?
//////////////////////////////////////////////////
Retract("CUnparsable?", All);
LocalSymbols(UnparseCAllFunctions) {

  /// predicate to test whether an expression can be successfully exported to C code

  /// interface with empty extra procedure list
  // need the backquote stuff b/c we have HoldArgument now
  CUnparsable?(expr_) <-- `CUnparsable?(@expr, []);

  // need to check that expr contains only allowed procedures
  CUnparsable?(expr_, funclist_List?) <--
  {
    Local(badprocedures);
    badprocedures := Difference(ProcedureList(expr), Concat(UnparseCAllFunctions, funclist));
    Decide(Length(badprocedures)=?0,
      True,
      {
        Decide(Verbose?(),
          Echo("CUnparsable?: Info: unexportable procedure(s): ", badprocedures)
        );
        False;
      }
    );
  }
  HoldArgumentNumber("CUnparsable?", 1, 1);
  HoldArgumentNumber("CUnparsable?", 2, 1);

  /// This is a list of all procedure atoms which UnparseC can safely handle
  UnparseCAllFunctions := Concat(AssociationIndices(UnparseCMathFunctions()), AssociationIndices(UnparseCRegularOps()),
  // list of "other" (non-math) procedures supported by UnparseC: needs to be updated when UnparseC is extended to handle new procedures
  [
    "For",
    "While",
    "Sequence",
    "Nth",
    "Modulo",
    "Complex",
    "If",
    "Else",
    "++",
    "--",
  ]
  );


} // LocalSymbols(UnparseCAllFunctions)

%/mathpiper





%mathpiper_docs,name="CUnparsable?",categories="Programming Procedures;Input/Output;Predicates"
*CMD CUnparsable? --- check possibility to export expression to C++ code
*STD
*CALL
        CUnparsable?(expr)
        CUnparsable?(expr, funclist)
        
*PARMS

{expr} -- expression to be exported (this argument is not evaluated)

{funclist} -- list of "allowed" procedure names

*DESC

{CUnparsable?} returns {True} if the MathPiper expression {expr} can be exported
into C++ code. This is a check whether the C++ exporter {UnparseC} can be safely
used on the expression.

A MathPiper expression is considered exportable if it contains only procedures that can be translated into C++ (e.g. {ListToProcedure} cannot be exported). All variables and constants are considered exportable.

The verbose option prints names of procedures that are not exportable.

The second calling format of {CUnparsable?} can be used to "allow" certain procedure names that will be available in the C++ code.

*E.G.
In> CUnparsable?(Sin(a1)+2*Cos(b1))
Result: True

Verbose(CUnparsable?(1+func123(b1)))
Result: False
Side Effects:
CUnparsable?: Info: unexportable procedure(s): func123

This returned {False} because the procedure {func123} is not available in C++. We can
explicitly allow this procedure and then the expression will be considered
exportable:

In> CUnparsable?(1+func123(b1), [func123])
Result: True;

*SEE UnparseC, Verbose                                                   
%/mathpiper_docs
                         




%mathpiper,name="CUnparsable?",subtype="automatic_test"
Verify(
    CUnparsable?(e+Pi*Cos(A-B)/3-Floor(3.14)*2)
    , True
);

Verify(
    CUnparsable?(e+Pi*Cos(A-B)/3-Floor(3.14)*2+badfunc(x+y))
    , False
);

Verify(
    CUnparsable?(e+Pi*Cos(A-B)/3-Floor(3.14)*2+badfunc(x+y), ["badfunc"])
    , True
);

Verify(
    CUnparsable?({i:=0;While(i<?10){i++; a:=a+i;}})
    , True
);

Verify(
    CUnparsable?({i:=0;While(i<?10){i++; a:=a+i; [];}})
    , False
);
%/mathpiper