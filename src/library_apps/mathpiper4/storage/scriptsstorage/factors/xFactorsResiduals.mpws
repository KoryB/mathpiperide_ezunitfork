%mathpiper,def="xFactorsResiduals;trySQF;tryRealRoots;processRealRoots"

//Retract("xFactorsResiduals",*);
//Retract("trySQF",*);
//Retract("tryRealRoots",*);
//Retract("processRealRoots",*);

10 # xFactorsResiduals( residualList_List? ) <--
{
    Decide(InVerboseMode(),{NewLine(); Tell("Residuals",residualList);});
    Decide(InVerboseMode(),Tell("  --",content));
    Decide(InVerboseMode(),Tell("  --",factrs));
    Decide(InVerboseMode(),Tell("  --",factrsnew));
    Decide(InVerboseMode(),Tell("  --",residuals));
    Decide(InVerboseMode(),Tell("  -- original ",degrees));
    Local(resid,sqf,sqfGood,rrGood);
    
    //  ---  see if Square-Free factoring can find some factors

    residualList := trySQF(residualList);   //  hso
    
    Decide(InVerboseMode(),
      {
         Tell("   after trying SQF on all residuals");
         Tell("          ",sqfGood);
         Tell("          ",factrsnew);
         Tell("          ",residualList);
         NewLine();
      }
    );
    
    //  ---  see if there are any REAL roots to help with factoring

    tryRealRoots(residualList);
    
    Decide(InVerboseMode(),
      {
         Tell("   after trying for REAL roots on all residuals");
         Tell("          ",rrGood);
         Tell("          ",factrsnew);
         Tell("          ",residuals);
         Tell("          ",residualList);
         NewLine();
      }
    );

     residOut;
};
UnFence("xFactorsResiduals",1);



10 # trySQF( residualList_List? ) <--
{
    //--- First, try SquareFree factorization on these residuals 
    Local(resid,sqf);
    Decide(InVerboseMode(),{NewLine(); Tell("trySQF",residualList);});
    ForEach(resid,residualList)
    {
        Decide(InVerboseMode(),Tell("   ",resid));
        
        sqf := SquareFree(resid[1]);
        Decide(InVerboseMode(),
          {
             Tell("          trying SQF");
             Tell("            ",resid[1]);
             Tell("            ",sqf);
          }
        );
        Decide(Degree(sqf) <? Degree(resid[1]),
          {
            Decide(InVerboseMode(),Tell("               sqf helps factor resid"));
            sqfGood := True;
            Local(f1,f2);
            f1 := sqf;
            f2 := Simplify(resid[1]/sqf);
            Decide( f2 =? f1,
                 factrsnew := Concat([[f1,2*resid[2]]],factrsnew),
                 factrsnew := Concat([[f1,resid[2]],[f2,resid[2]]],factrsnew)
            );
            //HSO experimental
            residuals := Difference(residuals,[resid]);
            Decide(InVerboseMode(),Tell("                       new",residuals));
            residualList := residuals;
          },
          {
            Decide(InVerboseMode(),
              {
                 Tell("               sqf DOES NOT HELP factor resid");
                 sqfGood := False;
              }
            );
          }
        );
        Decide(InVerboseMode(),Tell("            after sqf ",factrsnew));
        Decide(InVerboseMode(),Tell("                      ",residuals));
        Decide(InVerboseMode(),Tell("                      ",residualList));  // hso
    };
    residualList;   //  hso
};
UnFence("trySQF",1);



10 # tryRealRoots(residualList_List?)_(Length(residualList)>?0) <--
{
    //--- See if there are any REAL roots to factor out
    Decide(InVerboseMode(),{NewLine(); Tell("tryRealRoots",residualList);});
    ForEach(resid,residualList)
    {
        Local(nrr,rr,ptry,uptry);
        nrr := RealRootsCount(resid[1]);
        Decide(InVerboseMode(),
          { Tell("            this ",resid[1]); Tell("            ",nrr); }
        );
        Decide( nrr >? 0, rr := FindRealRoots(resid[1]), rr := [] );
        processRealRoots(rr);
        
        Decide( nrr =? 2,
          {
             Decide( nrr =? 0,
               {
                  //  OhOh - no real solutions -- have to try something Else
                  Decide(InVerboseMode(),
                    {
                       NewLine();
                       Tell("               NO real solutions");
                       Tell("               try something Else");
                    }
                  );
                  //  Here go some ad-hoc solutions that can be useful....
                  Local(u,X);
                  u := MakeUni(resid[1]);
                  X := u[1];
                  Decide( u[2]=?0 And? u[3]=?[1,0,1,0,1],
                    { 
                       Append!(residOut,[X^2-X+1,1]);
                       Append!(residOut,[X^2+X+1,1]);
                       Decide(InVerboseMode(),
                         {
                            Tell("     found ",factrsnew);
                            Tell("           ",resid);
                            Tell("           ",factrs);
                            Tell("           ",residOut);
                         }
                       );
                    }
                  );
               },
               {
                  //  more than 2 real solutions -- have to do a bit more work
                  rr := FindRealRoots(resid[1]);
                  Decide(InVerboseMode(),Tell("    ",rr));
                  // try them pairwise
                  goodptry := [];
                  For(ii:=1,ii<?nrr,ii++)
                      For(jj:=ii+1,jj<=?nrr,jj++)
                      {
                          Decide(InVerboseMode(),Tell("   ",[ii,jj]));
                          ptry     := Expand((x-rr[ii])*(x-rr[jj]));
                          uptry    := MakeUni(ptry);
                          uptry[3] := "NearRational" /@ uptry[3];
                          Decide(InVerboseMode(),Tell("    rat",uptry[3]));
                          Decide(InVerboseMode(),Tell("  ",Maximum(Denominator /@ uptry[3])));
                          Decide( Maximum(Denominator /@ uptry[3]) <? 100,
                            { Append!(goodptry,NormalForm(uptry)); }    );
                      };
                  Decide(InVerboseMode(),Tell("    ",goodptry));
                  Decide(Length(goodptry) >? 0,
                    {
                       ForEach(pt,goodptry)
                         { Append!(residOut,[pt,1]); };
                    }
                  );
               }
             );  //   if nrr=?0
          }
        );  //  if nrr=?2
     };  
};
UnFence("tryRealRoots",1);


10 # processRealRoots( rr_NumericList? )_(Length(rr) =? 1) <--
{
    //  Only one real root, so it will probably be of no help
    //  in factoring, unless it is integer or small rational
    Decide(InVerboseMode(),Tell("  Only 1 real root",rr));
    Local(root);
    root := rr[1];
    rrGood := False;
    Decide(Integer?(root), 
      {
          Decide(InVerboseMode(),Tell("    integer ",root));
          rrGood := True;
      },
      {
          Local(rroot);
          rroot := NearRational(root);
          Decide(InVerboseMode(),Tell("    rational ",rroot));
          Decide(Denominator(rroot) <? 100, {root := rroot; rrGood:=True;} );
      }
    );
    
};
UnFence("processRealRoots",1);


10 # processRealRoots( rr_NumericList? )_(Length(rr) =? 2) <--
{
    // a pair of real solutions -- probably form a quadratic
    ptry  := Expand((x-rr[1])*(x-rr[2]));
    Decide(InVerboseMode(),{Tell("    ",rr);Tell("    ",ptry);});
    uptry    := MakeUni(ptry);
    uptry[3] := "NearRational" /@ uptry[3];
    ptry     := NormalForm(uptry);
    Decide(InVerboseMode(),Tell("    ",ptry));
    Decide( Abs(Lcm(uptry[3])) <? 100,
      {
         //  looks OK -- try to use it
         Local(f1,f2,new);
         f1    := ptry;
         f2    := Simplify(resid[1]/f1);
         new   := [[f1,resid[2]],[f2,resid[2]]];
         Decide(InVerboseMode(),Tell("    ",new));
         resid := new;
         residOut := new;
         Decide(InVerboseMode(),Tell("    ",residOut)); 
      }
    );
};
UnFence("processRealRoots",1);



10 # processRealRoots( rr_NumericList? )_(Length(rr) >=? 4) <--
{
     //  more than 2 real solutions -- have to do a bit more work
     Decide(InVerboseMode(),Tell("    ",rr));
     // try them pairwise
     goodptry := [];
     For(ii:=1,ii<?nrr,ii++)
         For(jj:=ii+1,jj<=?nrr,jj++)
         {
             Decide(InVerboseMode(),Tell("   ",[ii,jj]));
             ptry     := Expand((x-rr[ii])*(x-rr[jj]));
             uptry    := MakeUni(ptry);
             uptry[3] := "NearRational" /@ uptry[3];
             Decide(InVerboseMode(),Tell("    rat",uptry[3]));
             Decide(InVerboseMode(),Tell("  ",Maximum(Denominator /@ uptry[3])));
             Decide( Maximum(Denominator /@ uptry[3]) <? 100,
               { Append!(goodptry,NormalForm(uptry)); }    );
         };
         Decide(InVerboseMode(),Tell("    ",goodptry));
         Decide(Length(goodptry) >? 0,
           {
               ForEach(pt,goodptry)
               { Append!(residOut,[pt,1]); };
           }
         );
     };


%/mathpiper





%mathpiper_docs,name="xFactorsResiduals",categories="Mathematics Functions",access="undocumented"
*CMD xFactorsResiduals --- *** UNDOCUMENTED ***

*CALL
        ?

*PARMS

?

*DESC

?

%/mathpiper_docs