%mathpiper,def="xFactors;xFactorsPrimitivePart;xFactorsUnivariate;xFactorsMultivariate;xFactorsHomogeneousBivariate;CheckForSpecialForms;ApproximateRealCoefficients;CombineNumericalFactors;IsPureRational;HasRealCoefficients;HasRationalCoefficients;FixUpMonicFactors;IsIrreducible;matchPower;matchPower"

/*------------------------------------------------------------------------
 *  PENULTIMATE VERSION 
 *    Started 091222
 *    revised 100108-22
 *    revised 100215
 *    revised 100301
 *------------------------------------------------------------------------*/

//Retract("xFactors",*);
//Retract("xFactorsPrimitivePart",*);
//Retract("xFactorsUnivariate",*);
//Retract("xFactorsMultivariate",*);
//Retract("xFactorsHomogeneousBivariate",*);
//Retract("CheckForSpecialForms",*);

//Retract("ApproximateRealCoefficients",*);
//Retract("FixUpMonicFactors",*);
//Retract("CombineNumericalFactors",*);
//Retract("IsPureRational",*);
//Retract("HasRealCoefficients",*);
//Retract("HasRationalCoefficients",*);
//Retract("matchPower",*);
//Retract("IsIrreducible",*);     //      NOT YET OPERATIONAL


/* --------------    LISTS    ---------------*/

10 # xFactors( L_List? ) <-- xFactors /@ L;


/* -------------    NUMBERS    --------------*/


10 # xFactors(p_PositiveInteger?) <--  
{
    Decide( p <? 1600, FactorsSmallInteger(p), FactorizeInt(p) );
};


12 # xFactors(p_NegativeInteger?) <-- xFactors(-p);


14 # xFactors(p_Rational?)_(Denominator(p) !=? 1) <-- 
             [ [xFactor(Numerator(p)) / xFactor(Denominator(p) ) , 1] ]; 
             

16 # xFactors(p_GaussianInteger?)        <-- GaussianFactors(p);


18 # xFactors(_p)_(Length(VarList(p))=?0) <-- [[p,1]];



/* -------------    POLYNOMIALS -- CAN BE UNI   --------------*/

21 # xFactors( poly_CanBeUni ) <--
{
    Decide(InVerboseMode(),Tell("xFactors_can_be_uni_100122",poly));
    Local(content,pp,ppFactors,monomialFactors,result);
    Local(vars,nvars,disassem,degrees,mpoly,nterms,allCoeffs,allPowers);
    Local(hasRealCoefficients,hasRationalCoefficients,isHomogeneous);

    //  First, check to see if the polynomial has any REAL coefficients.
    //  If so, convert to approximate integers (with divisor).
    hasRealCoefficients := HasRealCoefficients(poly);
    Decide( hasRealCoefficients, 
      {
         Local(realPoly);
         realPoly  := poly;   //  just in case -- save original form
         disassem  := DisassembleExpression(poly);
         allCoeffs := disassem[3];
         allPowers := Flatten(disassem[2],"List");
         poly      := ApproximateRealCoefficients(poly);
      }
    );
    
    //  Also, check to see if the polynomial has Rational coeffs
    hasRationalCoefficients := HasRationalCoefficients(poly);
    
    // Now: get Content and Primitive Part
    content := xContent( poly );
    pp      := xPrimitivePart( poly, content );
    Decide(InVerboseMode(),Tell("  ",[content,pp]));
    vars     := VarList(pp);
    nvars    := Length(vars);
    disassem := DisassembleExpression(pp);
    nterms   := Length(disassem[3]);
    degrees  := [];
    allCoeffs         := disassem[3];
    allPowers         := Flatten(disassem[2],"List");
    Decide(nvars >? 0,
      {
        ForEach(v,vars)
           { Append!(degrees,Degree(pp,v)); };
        isHomogeneous     := {
            // A polynomial is homogeneous of degree n 
            // if all terms have degree n.   
            Local(sd,cmp); 
            sd  := Sum /@ disassem[2];
            cmp := FillList(sd[1],Length(sd));
            ZeroVector?(sd - cmp);
        };
      }
    );
    
    // Experimental: 
    //   Attach a set of Meta-Keys to pp, describing
    //   some of the above information
    pp := MetaSet(pp,"nvars",nvars);
    pp := MetaSet(pp,"nterms",nterms);
    pp := MetaSet(pp,"degrees",degrees);
    pp := MetaSet(pp,"isHomogeneous",isHomogeneous);
    
    Decide(InVerboseMode(),
      {
        Tell("               ",vars);
        Tell("              ",nvars);
        Tell("             ",nterms);
        Tell("            ",degrees);
        Tell("           ",disassem);
        Tell("          ",allCoeffs);
        Tell("          ",allPowers);
        Tell("      ",isHomogeneous);
        NewLine();
      }
    );
    
    //  OK.  Now factor the PrimitivePart
    ppFactors := xFactorsPrimitivePart( pp );

    Decide(InVerboseMode(),{NewLine();Tell("  ",ppFactors);});
    
    // Next, include the factors of the Content, if any
    Decide(InVerboseMode(),NewLine());
    monomialFactors := FactorsMonomial(content);
    Decide(InVerboseMode(),{Tell("  ",monomialFactors);});
    Decide( monomialFactors[1][1] =? 1,
        result := ppFactors,        
        result := Concat(monomialFactors,ppFactors)
    );
    
    Decide(InVerboseMode(),{NewLine();Tell("  final  ",result);});
    result;
};



// -----------------  FACTOR PRIMITIVE PART  -----------------

     //  special case: binomials
10 # xFactorsPrimitivePart( _pp )_(nterms=?2) <--
{
    Decide(InVerboseMode(),Tell("Binomial"));
    Local(ppFactors);
    ppFactors := xFactorsBinomial(pp);
};
UnFence("xFactorsPrimitivePart",1);


     //  special case: homogeneous bivariates
12 # xFactorsPrimitivePart( _pp )_(isHomogeneous And? nvars=?2) <--
{
    Decide(InVerboseMode(),Tell("Homogeneous and Bivariate"));
    Local(ppFactors);
    ppFactors := xFactorsHomogeneousBivariate(disassem);
};
UnFence("xFactorsPrimitivePart",1);


     //  special case:  no variables in pp!
14 # xFactorsPrimitivePart( _pp )_(nvars=?0) <--
{
    Local(ppFactors);
    ppfactors := [];
};


     //  general case: univariate
16 # xFactorsPrimitivePart( _pp )_(nvars=?1) <--  xFactorsUnivariate(pp);
UnFence("xFactorsPrimitivePart",1);


     //  general case: multivariate
18 # xFactorsPrimitivePart( _pp )_(nvars>?1) <-- xFactorsMultivariate(pp);
UnFence("xFactorsPrimitivePart",1);


     //  catch-all: represents an ERROR CONDITION
20 # xFactorsPrimitivePart( _pp ) <-- Tell("Should never get here!");
UnFence("xFactorsPrimitivePart",1);



// ----------------------  UNIVARIATE POLYNOMIALS  -----------------------

30 # xFactorsUnivariate( poly_CanBeUni )_(Length(VarList(poly))=?1) <--
{
    Local(factrs,coeffs,deg,X,residuals,factrsnew);

    Decide(InVerboseMode(),
      {
         NewLine();
         Tell("xFactorsUnivariate",poly);
         Tell("       ",allCoeffs);
      }
    );
    
    // OK, First, send it through MathPiper's basic factoring function
    // for univariate polynomials
    
    factrs := BinaryFactors(poly);
    Decide(InVerboseMode(),Tell("   output of BinaryFactors",factrs));

    //    Now fix-up the (monic) factors found above, to express them
    // as linear in x with integer coefficients.  
    //    Also, separate out any 'residual' factors -- defined here as
    // factors of degree >? 2.
    
    [factrsnew,residuals] := FixUpMonicFactors(factrs);
    
    //  See if we can do something with the residuals
    Local(residOut);
    residOut := [];
    Decide(Length(residuals) >? 0, residOut := xFactorsResiduals( residuals ) );
    
    Decide(InVerboseMode(),
      {
          NewLine();
          Tell(" just before end of univariate factoring");
          Tell("    ",factrs);
          Tell("    ",factrsnew);
          Tell("    ",residOut);
      }
    );   
    //  Finally, the output --------
    Local(final);
    Decide(Length(Union(factrsnew,residOut)) >? 0,
        final := Concat(factrsnew,residOut),
        final := factrs
    );
    CheckForSpecialForms( final );
};   //   xFactorsUnivariate
UnFence("xFactorsUnivariate",1);


// ----------------  MULTIVARIATE POLYNOMIALS  -----------------

40 # xFactorsMultivariate( poly_CanBeUni )_(Length(VarList(poly))>?1) <--
{
    Local(factrs);
    
    Decide(InVerboseMode(),{NewLine();Tell("xFactorsMultivariate",poly);});
    Decide( nterms =? 2,
      {
        Decide(InVerboseMode(),Tell("   Is Binomial"));
        factrs := xFactorsBinomial(poly); 
      },
      {
        Decide(InVerboseMode(),Tell("   Has more than 2 terms"));
      }
    );
    factrs;
};
UnFence("xFactorsMultivariate",1);


// ------------------  HOMOGENEOUS BIVARIATE  ------------------

10 # xFactorsHomogeneousBivariate( dis_List? ) <-- 
{
    Decide(InVerboseMode(),{NewLine();Tell("xFactorsHomogeneousBivariate",dis);});
    Local(dis1,f,lst,dis2,poly1,ppFactors,residuals,ii,preassem);
    dis1  := [[xi],[[X],[X[1]]] /@ dis[2],dis[3]];
    Decide(InVerboseMode(),Tell("   ",dis1));
    poly1 := Sum(ReassembleListTerms(dis1));
    Decide(InVerboseMode(),Tell("   ",poly1));
    ppFactors := BinaryFactors(poly1);
    [ppFactors,residuals] := FixUpMonicFactors(ppFactors);
    For(ii:=1,ii<=?Length(ppFactors),ii++)
    {
        f := ppFactors[ii];
        Decide(InVerboseMode(),Tell("          ",f[1]));
        lst := DisassembleExpression(f[1]);
        Decide(InVerboseMode(),
          {
             Tell("               ",lst);
             Tell("                            ",dis[1]);
          }
        );
        DestructiveReplace(lst,1,dis[1]);
        Append!(lst[2][1],0);
        Append!(lst[2][2],1);
        Decide(InVerboseMode(),Tell("               ",lst));
        preassem  := Sum(ReassembleListTerms(lst)) ;
        Decide(InVerboseMode(),Tell("               ",preassem));
        ppFactors[ii][1] := preassem;
    };
    Decide(InVerboseMode(),{Tell("   ",ppFactors); Tell("   ",residuals);NewLine();} );  
    ppFactors;
};
UnFence("xFactorsHomogeneousBivariate",1);


// ------------------  SPECIAL FORMS  ------------------

10 # CheckForSpecialForms( final_List? ) <-- 
{
    Decide(InVerboseMode(),{NewLine();Tell("CheckForSpecialForms",final);});
    Local(LL,ii,fact,mult,dis,new);
    new := [];
    LL  := Length(final);
    For(ii:=1,ii<=?LL,ii++)
    {
        fact := final[ii][1];
        mult := final[ii][2];
        Decide(InVerboseMode(),Tell("   ",[fact,mult]));
        dis := DisassembleExpression( fact );
        Decide(InVerboseMode(),Tell("   ",dis));
        Local(var);
        var := dis[1][1];
        If( dis[2]=?[[4],[2],[0]] And? dis[3]=?[1,1,1] )
            {
                Local(new1,new2);
                new1 := [var^2-var+1,mult];
                new2 := [var^2+var+1,mult];
                Append!(new,new1);
                Append!(new,new2);
                Decide(InVerboseMode(),Tell("   ",new));
            }
        Else
            {
                Decide(InVerboseMode(),Tell("   no special form"));
                Append!(new,[fact,mult]);
            };
        );
    };
    new;
};


// ---------------------   OTHER STUFF ------------------------


10 # ApproximateRealCoefficients( poly_Polynomial? ) <--
{
    // If the polynomial has REAL coefficients, convert them to
    // approximate integers
    Decide(InVerboseMode(),{NewLine();Tell("  REAL",poly);});
    Local(coeffs,gcd,lcm);
    coeffs := Rationalize /@ (allCoeffs);
    Decide(InVerboseMode(),{Tell("      to-Q",coeffs);Tell("      to-Z",coeffs);});
    Local(gcd,lcm);
    gcd    := Gcd(Numerator /@ coeffs);
    lcm    := Lcm(Denominator /@ coeffs);
    Decide(InVerboseMode(),{Tell("       ",gcd);Tell("       ",lcm);});
    disassem[3] := coeffs;
    allCoeffs   := coeffs;
    poly        := Sum(ReassembleListTerms(disassem));
    Decide(InVerboseMode(),Tell("   new",poly));
    poly;
};
UnFence("ApproximateRealCoefficients",1);


100 # CombineNumericalFactors( factrs_List? ) <--
{
      Decide( InVerboseMode(), Tell("Combine",factrs) );
      Local(q,a,b,t,f,ff,err);
      err := False;
      t   := 1;
      f   := [];
      ForEach(q,factrs)
      {
          Decide( InVerboseMode(), Tell(1,q) );
          Decide( List?(q) And? Length(q)=?2,
            {
                [a,b] := q;
                Decide( InVerboseMode(), Echo("     ",[a,b]) );
                Decide( NumericList?( [a,b] ),
                    t := t * a^b,
                    f := [a,b]~f
                );
            },
              err := True
          );
      };
      Decide( InVerboseMode(),
        {
            Echo("      t = ",t);
            Echo("      f = ",f);
            Echo("    err = ",err);
        }
      );
      ff := Decide(Not? err And? t !=? 1, [t,1]~Reverse(f), factrs);
      ff := Select(Lambda([x],x!=?[1,1]),ff);
      Decide(ff[1]<?0,ff[1]:=-ff[1]);
};


// ----------------  RATIONAL POLYNOMIALS  -----------------

150 # xFactors( expr_RationalFunction? )_
        (Polynomial?(Numerator(expr)) And? Polynomial?(Denominator(expr))) <--
{
    Decide(InVerboseMode(),{NewLine();Tell("xFactors_Rational_Function",expr);});
    Local(Numer,Denom,fNumer,fDenom);
    Numer  := Numerator(expr);
    Denom  := Denominator(expr);
    fNumer := xFactors(Numer);
    fDenom := xFactors(Denom);
    Decide(InVerboseMode(),{Tell("   ",fNumer); Tell("   ",fDenom);});
    fNumer/fDenom;
};


152 # xFactors( expr_RationalFunction? )_
        (Constant?(Numerator(expr)) And? Polynomial?(Denominator(expr))) <--
{
    Decide(InVerboseMode(),{NewLine();Tell("xFactors_Rational_Denom",expr);});
    Local(Numer,Denom,fNumer,fDenom);
    Numer  := Numerator(expr);
    Denom  := Denominator(expr);
    fNumer := xFactors(Numer);
    fDenom := xFactors(Denom);
    Decide(InVerboseMode(),{Tell("   ",fNumer); Tell("   ",fDenom);});
    fNumer/fDenom;
};


// ----------   POSSIBLE NON-INTEGER EXPONENTS  ----------

200 # xFactors( _expr )_(Length(VarList(expr)) =? 1) <--
{
    Decide(InVerboseMode(),{NewLine();Tell("Some other kind of expression",expr);});
    Local(dis,X,pows);
    dis := DisassembleExpression(expr);
    X   := VarList(expr)[1];
    pows := matchPower /@ dis[1];
    rats := NearRational /@ pows;
    dis[1] := x^rats;
    p := Sum(ReassembleListTerms(dis));
    Decide(InVerboseMode(),Tell("    new ",p));
    xFactors(p);
};




10 # IsPureRational( N_Rational? )_(Not? Integer?(N)) <-- True;

12 # IsPureRational( _N ) <-- False;


10 # HasRealCoefficients( poly_Polynomial? ) <--
{
    Local(disassem);
    disassem := DisassembleExpression(poly);
    (Length(Select(disassem[3],"Decimal?")) >? 0);
};

10 # HasRealCoefficients( poly_Monomial? ) <--
{
    Local(disassem);
    disassem := DisassembleExpression(poly);
    (Length(Select(disassem[3],"Decimal?")) >? 0);
};


10 # HasRationalCoefficients( poly_Polynomial? ) <--
{
    Local(disassem);
    disassem := DisassembleExpression(poly);
    //Tell("              ",disassem);
    (Length(Select(disassem[3],"IsPureRational")) >? 0);
};

10 # HasRationalCoefficients( poly_Monomial?) <--
{
    Local(disassem);
    disassem := DisassembleExpression(poly);
    (Length(Select(disassem[3],"IsPureRational")) >? 0);
};


10 # FixUpMonicFactors( factrs_List? ) <--
{
    Decide(InVerboseMode(),{ NewLine(); Tell("   doing monic fixup"); } );
    Local(factrsnew,residuals,uni);
    factrsnew := [];
    residuals := [];
    ForEach(f,factrs)
    {
        Decide(InVerboseMode(),Tell("               ",f));
        uni := MakeUni(f[1]);
        Decide(InVerboseMode(),Tell("                    ",uni));
        Decide( Degree(f[1])=?1,
          {
            Local(cc,lcm,fnew);
            Decide(InVerboseMode(),Tell("                          ",Degree(f[1])));
            cc      := Coef(f[1],uni[1],0 .. 1);
            //Tell("                       ",cc);
            lcm     := Lcm( Denominator /@ cc );
            uni[3]  := lcm * cc;
            fnew    := NormalForm(uni);
            Decide( hasRationalCoefficients,
              {
                 Append!(factrsnew,f);
              },
              { 
                 Append!(factrsnew,[fnew,f[2]]);
              }
            );
          }
        );
        Decide( Degree(f[1])=?2,
          {
            Decide(InVerboseMode(),Tell("                          ",Degree(f[1])));
            Local(pq);
            pq      := PrimitivePart(f[1]);
            Append!(factrsnew,[pq,f[2]]);
          }
        );
        //  If any factors have degree >=3, store them in a 'residuals' array
        //  for further analysis
        Decide( Degree(f[1]) >? 2,
          {
            Decide(InVerboseMode(),Tell("                          ",Degree(f[1])));
            Local(pq);
            pq      := PrimitivePart(f[1]);
            Append!(residuals,[pq,f[2]]);
            Decide(InVerboseMode(),Tell("                   appending to residuals",pq));
          }
        );       
    };
    [factrsnew,residuals];
};
UnFence("FixUpMonicFactors",1);

10 # IsIrreducible( poly_Polynomial? )_(Length(VarList(poly))=?1) <--
{
    //  If these tests return True, the polynomial IS irreducible..
    //  If they return False, the reducibility of the polynomial is
    //  not established, one way or the other.
    //
    // ----   THIS FUNCTION IS NOT YET COMPLETE OR USEABLE   ---
    Decide(InVerboseMode(),Tell("IsIrreducible",poly));
    Local(var,deg,coeffs,num1);
    var    := VarList(poly)[1];
    deg    := Degree(poly);
    coeffs := Coef(poly,var,deg .. 0);
    Decide(InVerboseMode(),Tell("   ",deg));
    Local(ii,res,nprimes);
    nprimes := 0;
    For(ii:=-3*deg,ii<=?3*deg,ii:=ii+3)
    {
        res := NM(Substitute(x,ii) poly);
        //Tell("      ",[ii,res,Prime?(res)]);
        Decide(Abs(res)=?1 Or? Prime?(res), nprimes := nprimes + 1, );
    };
    Tell("   ",nprimes);
    Decide(nprimes >? 2*deg, True, False );
};


10 # matchPower(_Z^n_Number?) <-- n;

15 # matchPower(_Z) <-- 1;


//========================================================================

%/mathpiper





%mathpiper_docs,name="xFactors",categories="Mathematics Functions",access="undocumented"
*CMD xFactors --- *** UNDOCUMENTED ***

*CALL
        ?

*PARMS

?

*DESC

?

%/mathpiper_docs
