%mathpiper,def="Factors;FactorsMultivariateSpecialCases;FactorsSomethingElse;CombineNumericalFactors"


/*
 * This is the fundamental factorization algorithm as created for Yacas, with
 * a few improvements.
 * It works for integers, rational numbers, Gaussian integers, and polynomials
 * When the argument is an integer, FactorizeInt() does the heavy lifting.
 * When the argument is a polynomial, BinaryFactors() is the workhorse.
 */
 

10 # Factors(p_PositiveInteger?) <--  FactorizeInt(p);

11 # Factors(p_Integer?) <-- FactorizeInt(p);

12 # Factors(p_Rational?)_(Denominator(p) !=? 1) <-- [[YacasFactor(Numerator(p)) /YacasFactor(Denominator(p)) , 1]];
     //Added to handle rational numbers with denominators that are not 1

14 # Factors(p_GaussianInteger?)        <-- GaussianFactors(p);

20 # Factors(p_CanBeUni)_(Length(VarList(p)) =? 1) <--
{
    Local(x,d,coeffs,nterms,factorsList,result);
    x := VarList(p)[1];
    d := Degree(p,x);
    /*  p is the polynomial, x is its (only) variable.  It IS Univariate */
    /*  Let's find out how many terms this polynomial has.               */
    coeffs := Coef(p,x,0 .. Degree(p,x));
    nterms := Length(Select(coeffs, "NotZero?"));
    /*  If nterms = 2, it is a binomial, and might be most easily
     *  factored by some special-purpose algorithms                      */
    Decide( nterms =? 2 And? d >? 2,
      {  result := FactorsBinomials(p);  },    
      {  // nterms !=? 2, so try other techniques
        factorsList := BinaryFactors(p);
           // BinaryFactors is the internal MathPiper function that
           // creates a double list of factors and their multiplicities
        /*
         *  Now we check whether the input polynomial is "over the 
         *  integers", by examining all its coefficients
         */
        Decide( AllSatisfy?("Integer?",coeffs),
          {
              // Yes -- all integer coefficients
              result := FactorsPolynomialOverIntegers(p,x);
          },
          {
              // No -- at least one non-integer coefficient
              // Check for FLOAT or RATIONAL coefficients
              Local(notInt,rat,dd,lcm,newCoeffs,newPoly,facs);
              notInt := Select(coeffs, Lambda([i],Not? Integer?(i)));
              rat := Rationalize(coeffs);            
              dd  := MapSingle("Denominator",rat);
              lcm := Lcm(dd);
              newCoeffs := lcm * rat;
              newPoly := NormalForm(UniVariate(x,0,newCoeffs));
              facs := FactorsPolynomialOverIntegers(newPoly);
              Decide( InVerboseMode(), {
                  Echo("coeffs ",coeffs);
                  Echo("notInt ",notInt);
                  Echo("rat ",rat);
                  Echo("dd  ",dd);
                  Echo("lcm ",lcm);
                  Echo("newCoeffs ",newCoeffs);
                  Echo("newPoly ",newPoly);
                  Echo("facs ",facs);
                 }
              );
              result := [(1/lcm),1]~facs;
              //NOT FINISHED YET
        }
      );
    }
   );
      CombineNumericalFactors( result );
};


30 # Factors(p_CanBeUni) <--
{
    /* 
     * This may be a multi-variate polynomial, or it may be something Else.
     * Original YT function Factors() did not attempt to factor such.
     *    If it is a multivariate polynomial, we will try certain
     * Special cases which we can relatively easily factor.
     *    If it is "something Else", we will have to check, on a
     * case-by-case basis.
     */
    Local(vl,nvars,coeffs,result);
    vl     := VarList(p);
    nvars  := Length(vl);
    coeffs := Coef(p,x,0 .. 8);
    Decide(InVerboseMode(),Tell("CBU",[vl,nvars,coeffs]));
    Decide(nvars >? 1, 
         {
            Decide( InVerboseMode(), Echo(" special ",p));
            result := FactorsMultivariateSpecialCases(p);
         }, 
            result := FactorsSomethingElse(p) 
       );
       CombineNumericalFactors( result );
};


40 # Factors(_p) <--
{
    /*
     * This may may be a polynomial with non-integer exponents.  Let's check.
     */
     Decide( InVerboseMode(), Echo("Possibly trying to factor polynomial with non-integral exponents") );
     Local( result);
     //Echo(40,p);
     //  NOT IMPLEMENTED YET
     result := [[p,1]];
     CombineNumericalFactors( result );
     
};

//------------------------------------------------------------------------
//                    S P E C I A L     C A S E S
//------------------------------------------------------------------------

10 # FactorsMultivariateSpecialCases(-_expr) <-- [-1,1]~FactorsMultivariateSpecialCases(expr);

10 # FactorsMultivariateSpecialCases(x_Atom? + y_Atom?) <-- {Decide(InVerboseMode(),Tell(1));[[x+y,1]];};

10 # FactorsMultivariateSpecialCases(x_Atom? - y_Atom?) <-- {Decide(InVerboseMode(),Tell(2));[[x-y,1]];};

10 # FactorsMultivariateSpecialCases(_n*_x^p_Integer? + _n*_y) <-- {Decide(InVerboseMode(),Tell(3));[n,1]~FactorsMultivariateSpecialCases(x+y);};

10 # FactorsMultivariateSpecialCases(_n*_x^p_Integer? - _n*_y) <-- {Decide(InVerboseMode(),Tell(4));[n,1]~FactorsMultivariateSpecialCases(x-y);};

10 # FactorsMultivariateSpecialCases(n_Integer?*_x + m_Integer?*_y)_(Gcd(n,m)>?1) <-- [[Gcd(n,m),1],[(Simplify((n*x+m*y)/Gcd(n,m))),1]];

10 # FactorsMultivariateSpecialCases(n_Integer?*_x - m_Integer?*_y)_(Gcd(n,m)>?1) <-- [[Gcd(n,m),1],[(Simplify((n*x-m*y)/Gcd(n,m))),1]];

10 # FactorsMultivariateSpecialCases(_n*_x + _n*_y) <-- [n,1]~FactorsMultivariateSpecialCases(x+y);

10 # FactorsMultivariateSpecialCases(_n*_x - _n*_y) <-- [n,1]~FactorsMultivariateSpecialCases(x-y);

10 # FactorsMultivariateSpecialCases(_x^n_Integer? - _y) <-- FactorsBinomials(x^n - y); 

10 # FactorsMultivariateSpecialCases(_x^n_Integer? + _y) <-- FactorsBinomials(x^n + y); 

20 # FactorsSomethingElse(_p) <-- 
  {
      Decide( InVerboseMode(),
          {
              ECHO("   *** FactorsSomethingElse: NOT IMPLEMENTED YET ***");
          }
      );
      p;
  };

//------------------------------------------------------------------------


10 # CombineNumericalFactors( factrs_List? ) <--
  {
      Decide( InVerboseMode(), Tell("Combine",factrs) );
      Local(q,a,b,t,f,err);
      err := False;
      t   := 1;
      f   := [];
      ForEach(q,factrs)
      {
          Decide( InVerboseMode(), Tell(1,q) );
          Decide( List?(q) And? Length(q)=?2,
              {
                  [a,b] := q;
                  Decide( InVerboseMode(), Echo("     ",[a,b]) );
                  Decide( NumericList?( [a,b] ),
                      t := t * a^b,
                      f := [a,b]~f
                  );
              },
              err := True
          );
      };
      Decide( InVerboseMode(),
        {
            Echo("      t = ",t);
            Echo("      f = ",f);
            Echo("    err = ",err);
        }
      );
      Decide(Not? err And? t !=? 1, [t,1]~Reverse(f), factrs);
  };

%/mathpiper





%mathpiper_docs,name="Factors",categories="Mathematics Functions;Number Theory"
*CMD Factors --- factorization
*STD
*CALL
        Factors(x)

*PARMS

{x} -- integer or univariate polynomial

*DESC

This is mostly the original Yacas version of the function Factors(),
slightly modified for Mathpiper to improve some of its capabilities.
It has now been superceeded in MathPiper by the function xFactors(),
which has a large number of improvements.

This function decomposes the integer number {x} into a product of
numbers. 
Alternatively, if {x} is a univariate polynomial, it is
decomposed into irreducible polynomials.  If {x} is a polynomial
"over the integers", the irreducible polynomial factors will also
be returned in the (unique) form with integer coefficients.

The factorization is returned as a list of pairs. The first member of
each pair is the factor, while the second member denotes the power to
which this factor should be raised. So the factorization
$x = p1^n1 * ... * p9^n9$
is returned as {[[p1,n1], ..., [p9,n9]]}.

Programmer: Yacas Team + Sherm Ostrowsky

*E.G.

In> Factors(24)
Result: [[2,3],[3,1]]


In> Factors(32*x^3+32*x^2-70*x-75)
Result: [[4*x+5,2],[2*x-3,1]]

*SEE YacasFactor, xFactors, Prime?, GaussianFactors
%/mathpiper_docs




%mathpiper,name="Factors",subtype="automatic_test"

Testing("UnivariatePolynomialFactorization");

Verify(Factors(x^2-4),[[x-2,1],[x+2,1]]);
Verify(Factors(x^2+2*x+1),[[x+1,2]]);
Verify(Factors(-9*x^2+45*x-36),[[-9,1],[x-1,1],[x-4,1]]);
Verify(Factors(9*x^2-1),[[3,1],[x+1/3,1],[3*x-1,1]]);
Verify(Factors(4*x^3+12*x^2-40*x),[[4,1],[x,1],[x-2,1],[x+5,1]]);
Verify(Factors(32*x^3+32*x^2-70*x-75),[[32,1],[x-3/2,1],[x+5/4,2]]);
Verify(Factors(3*x^3-12*x^2-2*x+8),[[x-4,1],[3*x^2-2,1]]);
Verify(Factors(x^3+3*x^2-25*x-75),[[x+3,1],[x+5,1],[x-5,1]]);
Verify(Factors(2*x^3-30*x^2+12*x^4),[[12,1],[x,2],[x-3/2,1],[x+5/3,1]]);
Verify(Factors(5*x^7-20*x^6+25*x^5-20*x^4+25*x^3-20*x^2+20*x),[[5,1],[x,1],[x-2,2],[x^2+x+1,1],[x^2-x+1,1]]);
Verify(Factors((2/5)*x^2-2*x-(12/5)), [[2/5,1],[x+1,1],[x-6,1]]);
//Verify(Factors(.4*x^2-2*x-2.4), [[2/5,1],[x-6,1],[x+1,1]]); //Hangs.
Verify(xFactorsBinomial(x^3+1), [[x+1,1],[x^2-x+1,1]]);
Verify(xFactorsBinomial(x^4-1), [[x^2+1,1],[x-1,1],[x+1,1]]);
Verify(xFactorsBinomial(x^5-1), [[x-1,1],[x^4+x^3+x^2+x+1,1]]);
Verify(xFactorsBinomial(x^5+1), [[x+1,1],[x^4-x^3+x^2-x+1,1]]);


Testing("BivariatePolynomialFactorization");

Verify(Factors(-7*x-14*y),[[-7,1],[x+2*y,1]]);
Verify(xFactorsBinomial(x^2-a^2),[[x-a,1],[a+x,1]]);
Verify(xFactors(a^2+2*a*b+b^2),[[a+b,2]]);
Verify(xFactorsBinomial(x^3-y^3),[[x-y,1],[y^2+y*x+x^2,1]]);
Verify(xFactorsBinomial(x^3+a^3),[[a+x,1],[a^2-a*x+x^2,1]]);
Verify(xFactorsBinomial(x^6-a^6),[[a+x,1],[a^2-a*x+x^2,1],[x-a,1],[a^2+a*x+x^2,1]]);
Verify(xFactors(3*x^2-x*y-10*y^2),[[3*x+5*y,1],[x-2*y,1]]);

/* Bug #17 */
Verify(Assoc(x-1, xFactorsBinomial(x^6-1))[2], 1);


%/mathpiper


