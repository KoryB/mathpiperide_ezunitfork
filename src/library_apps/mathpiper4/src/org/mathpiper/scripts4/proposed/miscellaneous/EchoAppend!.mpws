%mathpiper,def="EchoAppend!;EchoAppendInternal!"

Retract("EchoAppend!", All);
Retract("EchoAppendInternal!", All);

?echoAppendList := [];


10 # EchoAppendInternal!(string_String?) <--
{
  Append!(?echoAppendList, string);
}

20 # EchoAppendInternal!(item_) <--
{
  Append!(?echoAppendList, item);
}



RulebaseListedHoldArguments("EchoAppend!",["firstParameter", "parametersList"]);

//Handle no option call.
5 # EchoAppend!(firstParameter_) <-- EchoAppend!(firstParameter, []);


//Main routine.  It will automatically accept 1 or more option calls because the
//options come in a list.
10 # EchoAppend!(firstParameter_, parametersList_List?) <--
{
    EchoAppendInternal!(firstParameter);
    ForEach(item,parametersList) EchoAppendInternal!(item);
    Append!(?echoAppendList, Nl());
    
}


//Handle a single option call because the option does not come in a list for some reason.
20 # EchoAppend!(firstParameter_, secondParameter_) <-- EchoAppend!(firstParameter, [secondParameter]);


//No argument EchoAppend! simply prints a newline.
EchoAppend!() := Append!(?echoAppendList, Nl());



%/mathpiper

    %output,mpversion=".209",preserve="false"
      Result: True
.   %/output



%mathpiper_docs,name="EchoAppend!",categories="Programming Procedures;Input/Output"
*CMD EchoAppend! --- prints items into a protected list
*STD
*CALL
        EchoAppend!(item,item,item,...)

*PARMS

{item} -- the items to be printed


*DESC

If passed a single item, {EchoAppend!} will evaluate it and append it to 
?echoAppendList, followed by a newline.

If {EchoAppend!} is called with a variable number of arguments, they will all
be appended and finally a newline will be appended.

If no arguments are passed to {EchoAppend!}, it will simply append a newline.

{EchoAppend!} always returns {True}.

*E.G.
In> ?echoAppendList := [];
Result: []

In> EchoAppend!(1,"Hello",1/2)
Result: [1,"Hello",1/2,"
"]

In> ?echoAppendList
Result: [1,"Hello",1/2,"
"]
 

*SEE Echo
%/mathpiper_docs

