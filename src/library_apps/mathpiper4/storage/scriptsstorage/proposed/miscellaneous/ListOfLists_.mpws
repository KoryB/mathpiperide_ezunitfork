%mathpiper,def="ListOfLists?"

ListOfLists?(listOfLists) :=
{
    Local(result);
    
    result := True;
    
    If(Not? List?(listOfLists))
    {
        result := False;
    }
    Else 
    {
        ForEach(list, listOfLists)
        {
            Decide(Not? List?(list), result := False);
        };
    };
    
    result;
};

%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output







%mathpiper_docs,name="ListOfLists?",categories="Programming Functions;Predicates",access="experimental"
*CMD ListOfLists? --- determine if [list] is a list of lists
*STD
*CALL
        List?(list)

*PARMS
{expr} -- a list

*DESC
This function returns {True} if {list} is a list of lists and {False} otherwise.

*E.G.
In> ListOfLists?(aa);
Result: False

In> ListOfLists?([1,2,3])
Result: False

In> ListOfLists?([[1,2],[3,4],[5,6]])
Result: True
    
%/mathpiper_docs
