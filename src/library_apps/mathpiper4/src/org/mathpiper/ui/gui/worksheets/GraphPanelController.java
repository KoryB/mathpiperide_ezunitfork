
package org.mathpiper.ui.gui.worksheets;

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;


public class GraphPanelController extends MathPanelController
{
    private JSlider zoomSlider;
    public GraphPanelController(ViewPanel viewPanel, double initialValue) {
        super(viewPanel, initialValue);

        zoomSlider = new JSlider(JSlider.HORIZONTAL, 0, 1000, (int) (50));
        zoomSlider.addChangeListener(this);

        //Turn on labels at major tick marks.
        //framesPerSecond.setMajorTickSpacing(10);
        //framesPerSecond.setMinorTickSpacing(1);
        //framesPerSecond.setPaintTicks(true);
        zoomSlider.setPaintLabels(true);

        this.add(new JLabel("Zoom"));
        
        this.add(zoomSlider);

    }
    
    public void stateChanged(ChangeEvent e) {

        JSlider source = (JSlider) e.getSource();
        
        if(source == zoomSlider)
        {
            int intValue = (int) source.getValue();
            
            // 10^(-50/25.)
            double axisValue = Math.pow(10, intValue / 50.0);
            
            PlotPanel plotPanel = (PlotPanel) viewPanel;
            
            plotPanel.xGraphMax = axisValue;
            
            plotPanel.yGraphMax = axisValue;
            
            plotPanel.repaint();
        }
        else
        {
            //if (!source.getValueIsAdjusting()) {
            int intValue = (int) source.getValue();
            double doubleValue = intValue / 10.0;
            //System.out.println("XXX: " + doubleValue);
            viewPanel.setViewScale(doubleValue);
            viewPanel.repaint();
        }
        //}
    }//end method.
}
