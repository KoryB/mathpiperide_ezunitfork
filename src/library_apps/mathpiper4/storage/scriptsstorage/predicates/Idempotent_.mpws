%mathpiper,def="Idempotent?"

Idempotent?(A_Matrix?) <-- (A^2 =? A);

%/mathpiper



%mathpiper_docs,name="Idempotent?",categories="Programming Functions;Predicates"
*CMD Idempotent? --- test for an idempotent matrix
*STD
*CALL
        Idempotent?(A)

*PARMS

{A} -- a square matrix

*DESC

{Idempotent?(A)} returns {True} if {A} is idempotent and {False} otherwise.
$A$ is idempotent iff $A^2=A$. Note that this also implies that $A$ raised
to any power is also equal to $A$.

*E.G.

In> Idempotent?(ZeroMatrix(10,10));
Result: True;

In> Idempotent?(Identity(20))
Result: True;
%/mathpiper_docs