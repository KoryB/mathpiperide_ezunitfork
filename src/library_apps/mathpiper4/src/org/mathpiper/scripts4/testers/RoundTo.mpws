%mathpiper,def="RoundTo"

/* Functions that aid in testing */

/* Round to specified number of digits */
10 # RoundTo(x_Number?, precision_PositiveInteger?) <--
{
  Local(oldPrec,result);

  oldPrec:=BuiltinPrecisionGet();

  BuiltinPrecisionSet(precision);

  Assign(result,DivideN( Round( MultiplyN(x, 10^precision) ), 10^precision ));

  BuiltinPrecisionSet(oldPrec);

  result;
}



// complex numbers too
10 # RoundTo(Complex(r_Number?, i_Number?), precision_PositiveInteger?) <-- Complex(RoundTo(r, precision), RoundTo(i, precision));




// Infinities, rounding does not apply.
20 # RoundTo( Infinity,precision_PositiveInteger?) <--  Infinity;

20 # RoundTo(-Infinity,precision_PositiveInteger?) <-- -Infinity;



/* ------   moved to separate file (already present but empty!) ---

Macro("NumericEqual",["left", "right", "precision"])
[
  Verify(RoundTo((@left)-(@right),@precision),0);
];

*/

%/mathpiper





%mathpiper_docs,name="RoundTo",categories="Programming Procedures;Testing"
*CMD RoundTo --- Round a real-valued result to a set number of digits
*STD
*CALL
        RoundTo(number,precision)

*PARMS

{number} -- number to round off

{precision} -- precision to use for round-off

*DESC

The procedure {RoundTo} rounds a floating point number to a
specified precision, allowing for testing for correctness
using the {Verify} command.

*E.G.

In> NM(RoundTo(Exp(1),30),30)
Result: 2.71828182110230114951959786552;

In> NM(RoundTo(Exp(1),20),20)
Result: 2.71828182796964237096;

*SEE Verify, VerifyArithmetic, VerifyDiv

%/mathpiper_docs