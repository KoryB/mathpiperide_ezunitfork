%mathpiper,def="FuncList"

//////////////////////////////////////////////////
/// FuncList --- list all function atoms used in an expression
//////////////////////////////////////////////////
/// like VarList except collects functions

10 # FuncList(expr_Atom?) <-- [];
20 # FuncList(expr_Function?) <--
RemoveDuplicates(
        Concat(
                [First(FunctionToList(expr))],
                Apply("Concat",
                        MapSingle("FuncList", Rest(FunctionToList(expr)))
                )
        )
);

/*
This is like FuncList except only looks at arguments of a given list of functions. All other functions become "opaque".

*/
10 # FuncList(expr_Atom?, looklist_List?) <-- [];
// a function not in the looking list - return its type
20 # FuncList(expr_Function?, looklist_List?)_(Not? Contains?(looklist, ToAtom(Type(expr)))) <-- [ToAtom(Type(expr))];
// a function in the looking list - traverse its arguments
30 # FuncList(expr_Function?, looklist_List?) <--
RemoveDuplicates(
        Concat(
                [First(FunctionToList(expr))],
                {        // gave up trying to do it using Map and MapSingle... so writing a loop now.
                        // obtain a list of functions, considering only functions in looklist
                        Local(item, result);
                        result := [];
                        ForEach(item, expr) result := Concat(result, FuncList(item, looklist));
                        result;
                }
        )
);

HoldArgumentNumber("FuncList", 1, 1);
HoldArgumentNumber("FuncList", 2, 1);

%/mathpiper



%mathpiper_docs,name="FuncList",categories="Programming Functions;Lists (Operations)"
*CMD FuncList --- list of functions used in an expression
*CMD FuncListArith --- list of functions used in an expression
*CMD FuncListSome --- list of functions used in an expression
*STD
*CALL
        FuncList(expr)
        FuncListArith(expr)
        FuncListSome(expr, list)

*PARMS

{expr} -- an expression

{list} -- list of function atoms to be considered "transparent"

*DESC

The command {FuncList(expr)} returns a list of all function atoms that appear
in the expression {expr}. The expression is recursively traversed.

The command {FuncListSome(expr, list)} does the same, except it only looks 
at arguments of a given {list} of functions. All other functions become 
"opaque" (as if they do not contain any other functions).
For example, {FuncListSome(a + Sin(b-c))} will see that the expression has 
a "{-}" operation and return {[+,Sin,-]}, but {FuncListSome(a + Sin(b-c), {+})} 
will not look at arguments of {Sin()} and will return {[+,Sin]}.

{FuncListArith} is defined through {FuncListSome} to look only at arithmetic operations {+}, {-}, {*}, {/}.

Note that since the operators "{+}" and "{-}" are prefix as well as infix operators, 
it is currently required to use {ToAtom("+")} to obtain the unevaluated atom "{+}".

*E.G. notest

In> FuncList(x+y*Cos(Ln(x)/x))
Result: [+,*,Cos,/,Ln];

In> FuncListArith(x+y*Cos(Ln(x)/x))
Result: [+,*,Cos];

In> FuncListSome([a+b*2,c/d],[List])
Result: [List,+,/];

*SEE VarList, HasExpression?, HasFunction?
%/mathpiper_docs