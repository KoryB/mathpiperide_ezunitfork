%mathpiper,def="PositionStringToList"

PositionStringToList(positionString) :=
{
    Local(result);

    Check(String?(positionString), "The argument must be a string.");
    
    If(positionString =? "")
    {
        result := [];
    }
    Else
    {
        result := MapSingle("StringToNumber", StringSplit(positionString, ","));
    }

    result;
}

%/mathpiper





%mathpiper_docs,name="PositionStringToList",categories="Programming Procedures;Expression Trees;Strings"
*CMD PositionStringToList --- convert the string version of an expression tree position list into a list

*CALL
    PositionStringToList(positionString)

*PARMS

{positionString} -- a position list in string form

*DESC

Convert the string version of an expression tree position list into a list

*E.G.
In> PositionStringToList("1,2,1,2")
Result: [1,2,1,2]

*SEE ListToString
%/mathpiper_docs





%mathpiper,name="PositionStringToList",subtype="automatic_test"

Verify(PositionStringToList(""), []);
Verify(PositionStringToList("1,2,1,2"), [1,2,1,2]);

%/mathpiper
