%mathpiper,def="Pslq"

/*********************************************************************************************#
#                       The PSLQ Integer Relation Algorithm                                   #
#                                                                                             #
# Aut.: Helaman R.P. Ferguson and David Bailey "A Polynomial Time, Numerically Stable         #
#       Integer Relation Algorithm" (RNR Technical Report RNR-92-032)    helaman@super.org    #
# Ref.: David Bailey and Simon Plouffe "Recognizing Numerical Constants" dbailey@nas.nasa.gov #
# Cod.: Raymond Manzoni  raymman@club-internet.fr                                             #
#*********************************************************************************************#
# Creation:97/11                    #
# New termination criteria:97/12/15 #
# this code is free...              #

Ported to MathPiper 2000 Ayal Pinkus.

Given a list of constants x find coefficients sol[i] such that
      sum(sol[i]*x[i], i=1..n) = 0    (where n=Length(x))

    x is the list of real expressions
          NM(x[i]) must evaluate to floating point numbers!
    precision is the number of digits needed for completion;
          must be greater or equal to log10(max(sol[i]))*n
    returns the list of solutions with initial precision
          and the confidence (the lower the better)

    Example:

    In> Pslq([2*Pi-4*Exp(1),Pi,Exp(1)],20)
    Result: [1,-2,4];

*/

Pslq(x, precision) :=
{
  Local (ndigits, gam, A, B, H, n, i, j, k, s, y, tmp, t, m, maxi, gami,
         t0, t1, t2, t3, t4, mini, Confidence, norme,result);
  n:=Length(x);
  ndigits:=BuiltinPrecisionGet();
  BuiltinPrecisionSet(precision+10); // 10 is chosen arbitrarily, but should always be enough. Perhaps we can optimize by lowering this number
  Confidence:=10^(-FloorN(NM(Eval(precision/3))));
//Echo("Confidence is ",Confidence);

  gam:=NM(Sqrt(4/3));
  For (i:=1, i<=?n,i++) x[i]:=NM(Eval(x[i]));

//Echo("1...");

  A:=Identity(n); /*A and B are of Integer type*/
  B:=Identity(n); /*but this doesn't speed up*/
  s:=ZeroVector(n);
  y:=ZeroVector(n);

//Echo("2...");

  For(k:=1,k<=?n,k++)
  {
    tmp:=0;
    For (j:=k,j<=?n,j++) tmp:=tmp + NM(x[j]^2);
//tmp:=DivideN(tmp,1.0);
//Echo("tmp is ",tmp);
//MathDebugInfo(tmp);
/*Decide(Not? PositiveNumber?(tmp),
  Echo("******** not a positive number: ",tmp)
);
Decide(Not? Number?(tmp),
  Echo("******** not a number: ",tmp)
);
Decide(LessThan?(tmp,0),
[
  Echo("******** not positive: ",tmp);
]
);*/

    s[k]:=SqrtN(tmp);


/*Decide(Not? Number?(tmp),
[
Echo("************** tmp = ",tmp);
]);
Decide(Not? Number?(s[k]),
[
Echo("************** s[k] = ",s[k]);
]);*/

  };

//Echo("3...");

  tmp:=NM(Eval(s[1]));
/*Decide(Not? Number?(tmp),
[
Echo("************** tmp = ",tmp);
]);*/

  For (k:= 1,k<=? n,k++)
  {
    y[k]:=NM(Eval(x[k]/tmp));
    s[k]:=NM(Eval(s[k]/tmp));

//Echo("1..."," ",y[k]," ",s[k]);
/*Decide(Not? Number?(y[k]),
[
Echo("************** y[k] = ",y[k]);
]);
Decide(Not? Number?(s[k]),
[
Echo("************** s[k] = ",s[k]);
]);*/

  };
  H:=ZeroMatrix(n, n-1);

//Echo("4...",n);
  For (i:=1,i<=? n,i++)
  {

    If(i <=? n-1)  { H[i][i]:=NM(s[i + 1]/s[i]); };

//Echo("4.1...");
    For (j:= 1,j<=?i-1,j++)
    {
//Echo("4.2...");
      H[i][j]:= NM(-(y[i]*y[j])/(s[j]*s[j + 1]));
//Echo("4.3...");

/*Decide(Not? Number?(H[i][j]),
[
Echo("************** H[i][j] = ",H[i][j]);
]
);*/

    };
  };

//Echo("5...");

  For (i:=2,i<=?n,i++)
  {
    For (j:=i-1,j>=? 1,j--)
    {
//Echo("5.1...");
      t:=Round(H[i][j]/H[j][j]);
//Echo("5.2...");
      y[j]:=y[j] + t*y[i];
//Echo("2..."," ",y[j]);
      For (k:=1,k<=?j,k++) { H[i][k]:=H[i][k]-t*H[j][k]; };
      For (k:=1,k<=?n,k++)
      {
        A[i][k]:=A[i][k]-t*A[j][k];
        B[k][j]:=B[k][j] + t*B[k][i];
      };
    };
  };
  Local(found);
  found:=False;

//Echo("Enter loop");

  While (Not?(found))
  {
    m:=1;
//Echo("maxi 1...",maxi);
    maxi:=NM(gam*Abs(H[1][1]));
//Echo("maxi 2...",maxi);
    gami:=gam;
//Echo("3...");
    For (i:= 2,i<=? n-1,i++)
    {
      gami:=gami*gam;
      tmp:=NM(gami*Abs(H[i][i]));
      If(maxi <? tmp)
      {
        maxi:=tmp;
//Echo("maxi 3...",maxi);
        m:=i;
      };
    };
//Echo("4...",maxi);
    tmp:=y[m + 1];
    y[m + 1]:=y[m];
    y[m]:=tmp;
//Echo("3..."," ",y[m]);
//Echo("5...");
    For (i:= 1,i<=?n,i++)
    {
      tmp:=A[m + 1][ i];
      A[m + 1][ i]:=A[m][ i];
      A[m][ i]:=tmp;
      tmp:=B[i][ m + 1];
      B[i][ m + 1]:=B[i][ m];
      B[i][ m]:=tmp;
    };
    For (i:=1,i<=?n-1,i++)
    {
      tmp:=H[m + 1][ i];

      H[m + 1][ i]:=H[m][ i];
      H[m][ i]:=tmp;
    };
//Echo("7...");
    If(m <? n-1)
    {
      t0:=NM(Eval(Sqrt(H[m][ m]^2 + H[m][ m + 1]^2)));

      t1:=H[m][ m]/t0;
      t2:=H[m][ m + 1]/t0;

//      Decide(Zero?(t0),t0:=NM(Confidence));
//Echo("");
//Echo("H[m][ m] = ",NM(H[m][ m]));
//Echo("H[m][ m+1] = ",NM(H[m][ m+1]));

//Decide(Zero?(t0),[t1:=Infinity;t2:=Infinity;]);
//Echo("t0=",NM(t0));
//Echo("t1=",NM(t1));
//Echo("t2=",NM(t2));

      For (i:=m,i<=?n,i++)
      {
        t3:=H[i][ m];
        t4:=H[i][ m + 1];
//Echo("    t1 = ",t1);
//Echo("    t2 = ",t2);
//Echo("    t3 = ",t3);
//Echo("    t4 = ",t4);
        H[i][ m]:=t1*t3 + t2*t4;
//Echo("7.1... ",H[i][ m]);
        H[i][ m + 1]:= -t2*t3 + t1*t4;
//Echo("7.2... ",H[i][ m+1]);
      };
    };
//Echo("8...");
    For (i:= 1,i<=? n,i++)
    {
      For (j := Minimum(i-1, m + 1),j>=? 1,j--)
      {
        t:=Round(H[i][ j]/H[j][ j]);
//Echo("MATRIX",H[i][ j]," ",H[j][ j]);
//Echo("5... before"," ",y[j]," ",t," ",y[i]);
        y[j]:=y[j] + t*y[i];
//Echo("5... after"," ",y[j]);
        For (k:=1,k<=?j,k++) H[i][ k]:=H[i][ k]-t*H[j][ k];
        For (k:= 1,k<=?n,k++)
        {
          A[i][ k]:=A[i][ k]-t*A[j][ k];
          B[k][ j]:=B[k][ j] + t*B[k][ i];
        };
      };
    };
//Echo("9...",NM(H[1],10));

    /* BuiltinPrecisionSet(10);*/ /*low precision*/
//    maxi := NM(Dot(H[1],  H[1]),10);
    maxi := NM(Dot(H[1], H[1]));
//Echo("H[1] = ",H[1]);
//Echo("NM(H[1]) = ",NM(H[1]));
//Echo("NM(Dot(H[1], H[1])) = ",NM(Dot(H[1], H[1])));
//Echo("maxi 4...",maxi);

//Echo("9... maxi = ",maxi);

    For (j:=2,j<=?n,j++)
    {
//Echo("9.1...");
      tmp:=NM(Dot(H[j], H[j]),10);
//Echo("9.2...");
      If(maxi <? tmp) { maxi:=tmp; };
//Echo("maxi 5...",maxi);
//Echo("9.3...");
    };
//Echo("10...");
    norme:=NM(Eval(1/Sqrt(maxi)));
    m:=1;
    mini:=NM(Eval(Abs(y[1])));
//Echo("y[1] = ",y[1]," mini = ",mini);
    maxi:=mini;

//Echo("maxi 6...",maxi);
//Echo("11...");
    For (j:=2,j<=?n,j++)
    {
      tmp:=NM(Eval(Abs(y[j])));
      If(tmp <? mini)
      {
        mini:=tmp;
        m:=j;
      };
      If(tmp >? maxi) { maxi:=tmp; };
//Echo("maxi 7...",maxi);
    };
    /* following line may be commented */
//Echo(["Norm bound:",norme," Min=",mini," Conf=",mini/maxi," required ",Confidence]);
    If((mini/maxi) <? Confidence) /*prefered to : if mini <? 10^(- precision) then*/
    {
    /* following line may be commented */
/*      Echo(["Found with Confidence ",mini/maxi]); */
      BuiltinPrecisionSet(ndigits);
      result:=Transpose(B)[m];
      found:=True;
    }
    Else
    {
      maxi:=Abs(A[1][ 1]);
      For (i:=1,i<=?n,i++)
      {
//Echo("i = ",i," n = ",n);
        For (j:=1,j<=?n,j++)
        {
//Echo("j = ",j," n = ",n);
          tmp:=Abs(A[i][ j]);
          If(maxi <? tmp) { maxi:=tmp;};
        };
      };
//Echo("maxi = ",maxi);
      If(maxi >? 10^(precision))
      {
        BuiltinPrecisionSet(ndigits);
        result:=Fail;
        found:=True;
      };
      BuiltinPrecisionSet(precision+2);
//Echo("CLOSE");
    };
  };
  result;
};

/* end of file */

%/mathpiper



%mathpiper_docs,name="Pslq",categories="Mathematics Functions;Numbers (Operations)"
*CMD Pslq --- search for integer relations between reals
*STD
*CALL
        Pslq(xlist,precision)

*PARMS

{xlist} -- list of numbers

{precision} -- required number of digits precision of calculation

*DESC

This function is an integer relation detection algorithm. This means
that, given the numbers $x{i}$ in the list "xlist", it tries
to find integer coefficients $a{i}$ such that
$a{1}*x{1}$ + ... + $a{n}*x{n} = 0$.
The list of integer coefficients is returned.

The numbers in "xlist" must evaluate to floating point numbers if
the {N} operator is applied on them.

*E.G.

In> Pslq([ 2*Pi+3*Exp(1), Pi, Exp(1) ],20)
Result: [1,-2,-3];

Note: in this example the system detects correctly that
$1 * (2*Pi+3*e) + (-2) * Pi + (-3) * e = 0$.

*SEE N
%/mathpiper_docs





%mathpiper,name="Pslq",subtype="automatic_test"

VerifyPslq(left,right):=
{
  Decide(left=?right,
    Verify(True,True),
    `Verify(@left,-(@right)));
};

VerifyPslq(Pslq([ Pi+2*Exp(1) , Pi , Exp(1) ],20),[1,-1,-2]);
VerifyPslq(Pslq([ 2*Pi+3*Exp(1) , Pi , Exp(1) ],20),[1,-2,-3]);

%/mathpiper