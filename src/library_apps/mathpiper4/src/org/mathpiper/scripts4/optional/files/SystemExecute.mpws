%mathpiper,def="SystemExecute"


SystemExecute(command) :=
{
    SystemExecute(command, [], Null, True);
}


SystemExecute(command, environment, directory, waitFor?) :=
{
    Check(List?(command), "The first argument must be a list.");

    Check(List?(environment), "The second argument must be a list.");

    Check(directory =? Null |? String?(directory), "The third argument must be a string or Null.");
    
    Check(Boolean?(waitFor?), "The fourth argument must be True or False.");

    Local(runtime, reader, errorReader, process, resultLines, errorLines, file, commandCount, commandArray, index);
    
    commandCount := Length(command);
    
    commandArray := JavaCall("java.lang.reflect.Array", "newInstance", JavaCall(JavaNew("java.lang.String"),"getClass"), commandCount);
    
    For(index := 0, index <? commandCount, index++)
    {
        JavaCall("java.lang.reflect.Array", "set", commandArray, index, command[index+1]);
    }
    
    runtime := JavaCall("java.lang.Runtime", "getRuntime");

    If(directory !=? Null)
    {
        file := JavaNew("java.io.File", directory);

        Check(JavaAccess(file,"exists") =? True, "The file <" + directory + "> does not exist.");
    }
    Else
    {
            file := Null;
    }

    process := JavaCall(runtime, "exec", commandArray, Null, file);

    If(waitFor?)
    {
        JavaCall(process, "waitFor");
    
        reader := JavaNew("java.io.BufferedReader", JavaNew("java.io.InputStreamReader", JavaCall(process, "getInputStream")));
    
        errorReader := JavaNew("java.io.BufferedReader", JavaNew("java.io.InputStreamReader", JavaCall(process, "getErrorStream")));
    
        resultLines := ReadLines(reader);
    
        errorLines := ReadLines(errorReader);
    
        JavaCall(reader, "close");
    
        JavaCall(errorReader, "close");
    
        Check(errorLines =? [], "Errors: " + ToString(errorLines));
    
        resultLines;
    }
    Else
    {
        True;
    }
}

%/mathpiper





%mathpiper_docs,name="SystemExecute",categories="Programming Procedures;Input/Output",access="experimental"
*CMD SystemExecute --- executes a command in the system

*CALL
    SystemExecute(command)
        SystemExecute(command, environment, directory, waitFor?)

*PARMS
{command} -- a list of strings that contains the command to execute along with its arguments

{environment} -- a list that contains the environment to execute the command in

{directory} -- directory to execute the command in

{waitFor?} -- when True waits for the command to finish executing before returning

*DESC
This procedure executes a command in a system shell.

*E.G.
SystemExecute(["ls", "-la"])

%/mathpiper_docs
