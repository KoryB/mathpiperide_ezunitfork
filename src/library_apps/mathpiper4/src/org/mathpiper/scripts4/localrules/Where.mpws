%mathpiper,def="Where"

RulebaseHoldArguments("Where", ["left", "right"]);

UnFence("Where", 2);

10 # (body_ Where var_Atom? == value_) <-- 
`{ 
    //Local(@var);
    //@var := @value;
    //@body;
    
    Eval(Substitute(@var, @value) @body);
}
     
     
20 # (body_ Where (a_ &? b_)) <--
{
  Assign(body,`(@body Where @a));
  `(@body Where @b);
}

30 # (body_ Where []) <-- [];

40 # (body_ Where list_List?)::List?(list[1]) //todo:tk:this rule needs an example and a test.
     <--
     {
       Local(head,rest);
       head := First(list);
       rest := Rest(list);
       rest := `(@body Where @rest);
       Concat(`(@body Where @head), rest);
     }

50 # (body_ Where list_List?)
     <--
     {
       Local(head, rest, result);
       result := [];

       While (list !=? [])
       {
          head := First(list);
          result := Append(result, `(@body Where @head));
          list := Rest(list);
       }
       result;
     }


60 # (body_ Where var_ == value_) <-- Substitute(var,value)body;

%/mathpiper



%mathpiper_docs,name="Where",categories="Mathematics Procedures;Solvers (Symbolic)"
*CMD Where --- substitute result into expression
*STD
*CALL
        expr Where x == v
        expr Where x1 == v1 &? x2 == v2 &? ...
        expr Where [x1 == v1 &? x2 == v2, x1 == v3
          &? x2 == v4, ...]

*PARMS

{expr} - expression to evaluate

{x} - variable to set

{v} - value to substitute for variable

*DESC

The operator {Where} fills in values for variables, in its simplest form.
It accepts sets of variable/value pairs defined as 

        var1 == val1 &? var2 == val2 &? ...

and fills in the corresponding values. Lists of value pairs are
also possible, as:

        [var1 == val1 &? var2 == val2, var1 == val3 &? var2 == val4]

These values might be obtained through {Solve}.

This operator can help the user to program in the style of functional 
programming languages such as Miranda or Haskell.

*E.G.
In> _x^2 + _y^2 Where _x == 2
Result: 4 + _y^2

In> _x^2 + _y^2 Where _x == 2 &? _y == 3
Result: 13

In> _x^2 + _y^2 Where [_x == 2 &? _y == 3]
Result: [13]

In> _x^2 + _y^2 Where [_x == 2 &? _y == 3, _x == 4 &? _y == 5]
Result: [13,41]

*SEE Solve, AddTo, Substitute, Where, WithValue, /:
%/mathpiper_docs





%mathpiper,name="Where",subtype="automatic_test"

Verify(_x^2 + _y^2 Where _x == 2, 4 + _y^2);
Verify(_x^2 + _y^2 Where _x == 2 &? _y == 3, 13);
Verify(_x^2 + _y^2 Where [_x == 2 &? _y == 3], [13]);
Verify(_x^2 + _y^2 Where [_x == 2 &? _y == 3, _x == 4 &? _y == 5], [13,41]);

%/mathpiper