%mathpiper,def="Map"

LocalSymbols(func,lists,mapsingleresult,mapsingleitem)
{
  TemplateProcedure("Map",["func", "lists"])
  {
    Local(mapsingleresult,mapsingleitem);
    mapsingleresult:=[];
    lists:=Transpose(lists);
    ForEach(mapsingleitem,lists)
    {
      Insert!(mapsingleresult,1,Apply(func,mapsingleitem));
    }
    Reverse!(mapsingleresult);
  }
  UnFence("Map",2);
  HoldArgument("Map","func");
}

%/mathpiper



%mathpiper_docs,name="Map",categories="Programming Procedures;Lists (Operations)"
*CMD Map --- apply an $n$-ary procedure to all entries in a list
*STD
*CALL
        Map(fn, list)

*PARMS

{fn} -- procedure to apply

{list} -- list of lists of arguments

*DESC

This procedure applies "fn" to every list of arguments to be found in
"list". So the first entry of "list" should be a list containing
the first, second, third, ... argument to "fn", and the same goes
for the other entries of "list". The procedure can either be given as
a string or as a pure procedure (see Apply for more information on 
pure procedures).

*E.G.
For procedures needing TWO arguments
In> Map("+",[[_a,1/5],[Pi,4]]);
Result> [_a+Pi,21/5]

In> Map("List",[[1,2,3],[4,5,6]]);
Result: [[1,4],[2,5],[3,6]]

For procedures needing ONE argument
In> MapSingle("Sin",[_a,Pi,0]);
Result> [Sin(_a),Sin(Pi),Sin(0)]

*SEE MapSingle, MapArgs, Apply
%/mathpiper_docs