%mathpiper,def="GaussianUnit?"

GaussianUnit?(z_GaussianInteger?) <-- GaussianNorm(z)=?1;

%/mathpiper



%mathpiper_docs,name="GaussianUnit?",categories="Programming Functions;Predicates"
*CMD GaussianUnit? ---  test for a Gaussian unit
*STD
*CALL
        GaussianUnit?(z)
*PARMS

{z} -- a Gaussian integer        

*DESC

This function returns {True} if the argument is a unit in the Gaussian 
integers and {False} otherwise. A unit in a ring is an element that divides 
any other element.

There are four "units" in the ring of Gaussian integers, which are 
$1$, $-1$, $I$, and $-I$.
        
*E.G.
In> GaussianInteger?(I)
Result: True;

In> GaussianUnit?(5+6*I)
Result: False;

*SEE  GaussianInteger?, GaussianPrime?, GaussianNorm
%/mathpiper_docs